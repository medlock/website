#!/bin/bash
#
# indexer.sh
#
# Script to create entries in
#  index.md

# ALPHA - proof of concept!
#   
# Need to 
#   1) make more efficient and 
#   2) Define functions to reduce repeated code!
#   3) Remove hard-coded stuff        
#   4) Make more robust!
#
# -- Constants --
#

MODE="D"                                                                        # Production or Debug                                                                      # Debug
LEVEL="V"
#
SWNAME="indexer"
Version="0.0.0"
#

# temp file "rec"
tmpDate=""
tmpType=""
tmpFile=""
tmpTitle=""
tmpTags=""


#
# Where am I installed?
# all other relevant dirs are relative to this.
#
DIR=$(cd "$(dirname "$0")" && pwd)
ind=$(echo "$DIR" | grep -b -o "/scripts" | cut -d: -f1)
#ind=$((ind - 9))

SRC=${DIR:0:$ind}

#
# Functions
#
# Functions are in functions.sh, load it ...

. "$SRC"/scripts/functions.sh


#
## "Main" section
#

SYYMMDD=$(date +%Y-%m-%d)

# Purge old data and logs

rm -f "$SRC"/logs/job.log
rm -f "$SRC"/data.tmp
rm -f "$SRC"/data-sort.tmp


logit "START" "Version $Version - Mode/Level $MODE/$LEVEL"

#
# Process all .md files in docs and create temp file for subsequent date order 
# sorting. Temp file contails .md file name, title, date, & tags
#
# It gets this info, in meta data at the top of the file, encapsulated by ---
#
# eg
#     ---
#     title: Posts
#     ref: Modified 2022-01-20
#     tags: posts, test, 
#     ---
#

for file in "$SRC"/docs/*.md                                                      # Find all markdown files is dir
do
    nRec=0
    bMETA="false"
	
	tmpDate=""
	tmpType=""
	tmpFile=""
	tmpTitle=""
	tmpTags=""

    while [[ nRec -le 10 ]]                                                     # Max of 10 lines incl ---
    do                                                                          # 8 keywords
        read -r inRec                                                              # 

        if [[ ${inRec:0:3} == "---" && $bMETA == "false" ]]; then 
            bMETA="true"
        fi

        if [[ $bMETA == "true" && ${inRec:0:3} != "---" ]]; then
            
            len=${#inRec}
            pos=$(echo "$inRec" | grep -b -o ": " | cut -d: -f1)

            pos=$pos+2

            Defn=${inRec:0:$pos}
           
            if [[ $Defn == "title: " ]]; then
                tmpTitle=${inRec:$pos:$len}
            fi

            if [[ $Defn == "type: " ]]; then
                tmpType=${inRec:$pos:$len}
            fi

            if [[ $Defn == "ref: " ]]; then
                tmpDate=${inRec:$len-10:$len}                                   # date is at the eof string
            fi                                                                  # yyyy-mm-dd (10 bytes)
            
            if [[ $Defn == "tags: " ]]; then                                    # tags are delim by ","
                tmpTags=${inRec:$pos:$len}
            fi
        fi
  
        len=${#file}
        pos=$(echo "$file" | grep -b -o "/docs/" | cut -d: -f1)

        pos=$pos+5

        xxx=${file:$pos:$len}
        fileShort=${xxx:1:$len-3}

        if [[ ${inRec:0:3} == "---" && $bMETA == "true" && $nRec -gt 1 ]]; then 
            bMETA="false"
            nRec=11     
     #       logit "dbug5" "$SYYMMDD - $tmpDate"                                # META finished, jump out
            if [[ ! $tmpDate > $SYYMMDD ]]; then                                # No entries with future date
                echo "$tmpDate,$tmpType,$fileShort,$tmpTitle,[$tmpTags]" >> "$SRC"/data.tmp
            fi
            logit ">Pars" "$tmpDate $tmpType $fileShort $tmpTitle $tmpTags"
        fi
    
        ((nRec++))

    done < "$file"
done

logit ">sort" "Start"
    
sort -nr "$SRC"/data.tmp > "$SRC"/data-sort.tmp										# sort into descending
																				# date order
logit ">sort" "End"


#
# Update index.md 
# 

OutFile=$SRC/docs/index.md

nCnt=0

#OLDIFS=$IFS
IFS=','

LastModDate=""

logit ">indx" "Start"

# remove outdated index.md

rm -f "$OutFile"

AutoCmnt="false"

while read -r infile
do
    echo "$infile" >> "$OutFile"

    if [[ $infile == *"---"* && $AutoCmnt == "false" ]]; then
        echo "comment: AUTO GENERATED! - Edit template version of this file." >> "$OutFile"
        AutoCmnt="true"
    fi

done < "$SRC"/templates/index.md


nCnt=0

while [[ nCnt -lt 6 ]]
do 
    read -r tmpDate tmpType tmpFile tmpTitle tmpTags
    len=${#tmpFile}

    htmlFile=${tmpFile:0:$len-3}
	
    if [[ $tmpType == "post" ]]; then
        echo "  - [$tmpTitle ($tmpDate)](/docs/$htmlFile.html)" >> "$OutFile"
        
        ((nCnt++))																# incr for a post
    fi
	
	LastModDate=$tmpDate
	
	
done < "$SRC"/data-sort.tmp

echo " " >> "$OutFile"
echo "<br> " >> "$OutFile"

#ModDate=$(echo "$LastModDate" | awk -F'-' '{printf("%02d-%02d-%04d\n",$3,$2,$1)}') 

LastModDate=$(date --utc +"%d %b %Y @ %H:%M:%S")
 
echo "Last Updated $LastModDate " >> "$OutFile"

logit ">indx" "Stop"

logit "STOP!" "Version $Version - Mode/Level $MODE/$LEVEL"
