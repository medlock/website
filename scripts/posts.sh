#!/bin/bash
#
# posts.sh
#
# Script to create entries in posts.md

MODE="D"                                                                        # Production or Debug                                                                      # Debug
LEVEL="V"
#
SWNAME="posts"
Version="0.0.0"
#
#
# Where am I installed?
# all other relevant dirs are relative to this.
#
DIR=$(cd "$(dirname "$0")" && pwd)
ind=$(echo "$DIR" | grep -b -o "/scripts" | cut -d: -f1)
#ind=$((ind - 9))

SRC=${DIR:0:$ind}

#
# Functions
#
# Functions are in functions.sh, load it ...

. "$SRC"/scripts/functions.sh

SYYMMDD=$(date +%Y-%m-%d)

#
# Process each rec in tmp file, and add an entry in
# posts.md
#
# THIS CODE NEEDS TO BE A SERIES OF FUNCTIONS - REUSABLE
#

logit "posts  " "Start"

OutFile="$SRC"/docs/posts.md

AutoCmnt="false"



#
# Update posts.md 
# 

nCnt=0

IFS=','

LastModDate=""


while read -r tmpDate tmpType tmpFile tmpTitle tmpTags
do
    len=${#tmpFile}

    htmlFile=${tmpFile:0:$len-3}

	if [[ $tmpDate > $LastModeDate ]]; then										# Use this for site
		LastModDate=$tmpDate													# mod date
		if [[ $AutoCmnt == "false" ]]; then
			# Create new file and send new meta data
			echo "---" > "$OutFile"
			echo "title: Posts" >> "$OutFile"
			echo "ref: Modified $LastModDate " >> "$OutFile"
			echo "type: page" >> "$OutFile"
			echo "comment: AUTO GENERATED!" >> "$OutFile"
			echo "---" >> "$OutFile"
			echo " " >> "$OutFile"
			echo " " >> "$OutFile"
			echo "List of current posts:-" >> "$OutFile"
			echo " " >> "$OutFile"
			AutoCmnt="true"
		fi
	fi
	
    if [[ $tmpType == "post" ]]; then
        echo "- [$tmpTitle ($tmpDate)](/docs/$htmlFile.html)" >> "$OutFile"
        ((nCnt++))
    fi

done < "$SRC"/data-sort.tmp

echo " " >> "$OutFile"
echo " " >> "$OutFile"

echo "$nCnt entries" >> "$OutFile"

echo " " >> "$OutFile"
echo " " >> "$OutFile"


logit "posts  " "End"
