#!/bin/bash

# Python web server to check site is working as expected
# url http://localhost:8000/

python3 -m http.server --directory output/development
