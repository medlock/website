#!/bin/bash

# Use this script to deploy teh site using drone if you can't get drone 
# to process if statements in the pipeline.
#
# make-cleam.sem is a semphore to determine if make clean should be used.
# Use it to start a fresh
#
# run "make sem" to create it, or "touch ./make-ckean.sem"
#
# make clean is needed when -
# 1) new website
# 2) after a template, header, footer alteration to force it to regenerate with new 
#    alteration.


if [ -f ./make-clean.sem ]
then
    make clean
    rm -f ./make-clean.sem
fi

make