#!/bin/bash
#
# build.sh
#
# Script to build site

MODE="D"                                                                        # Production or Debug                                                                      # Debug
LEVEL="V"
#
SWNAME="build"
Version="0.0.1"
#


if [[ $# -ne 1 ]]; then
	echo S0: usage: build dev/live
	exit 1
fi

if  [[ $1 == "dev" ]]; then
	dev="true"
	TARGETDIR="output/development"
	echo "This is a development run"
else
	if [[ $1 == "live" ]]; then
		dev="false"
		TARGETDIR="output/production"
		echo "This is a production run"
	else
		echo $0: usage build dev or build live
		exit 1
	fi
fi
#
# Where am I installed?
# all other relevant dirs are relative to this.
#
DIR=$(cd "$(dirname "$0")" && pwd)
ind=$(echo "$DIR" | grep -b -o "/scripts" | cut -d: -f1)
#ind=$((ind - 9))

SRC=${DIR:0:$ind}

#
# Functions
#
# Functions are in functions.sh, load it ...

. "$SRC"/scripts/functions.sh


#
## "Main" section
#

SYYMMDD=$(date +%Y-%m-%d)


echo "Output will be in $TARGETDIR"
buildOK="false"

logit "START" "Version $Version - Mode/Level $MODE/$LEVEL"

echo "indexing ..."
"$SRC"/scripts/indexer.sh

echo "parsing posts ..."
"$SRC"/scripts/posts.sh

echo "parsing memoirs ..."
"$SRC"/scripts/memoirs.sh

echo "generating pages ..."

for file in "$SRC"/docs/*.md                                                    # Find all markdown files is dir
do
	len=${#file}
	TheFile=${file:0:len-3}
	len=${#TheFile}
	TheFileShort=${TheFile:47:len-3} # MAKE MORE ROBUST
#
# Check to see if file has been modified, if it has generate a new one
#
	if [[ -f "$SRC"/"$TARGETDIR"/docs/"$TheFileShort".html ]]; then 
		SRCFileDate=$(date -d @$( stat -c %Y $file ) +%Y%m%d%H%M%S )
		OUTFileDate=$(date -d @$( stat -c %Y "$SRC"/"$TARGETDIR"/docs/"$TheFileShort".html ) +%Y%m%d%H%M%S ) 
	else
		SRCFileDate=1
		OUTFileDate=0
	fi

	#echo $SRCFileDate
	#echo $OUTFileDate
	#echo $dev
	#echo $TARGETDIR
	
	if [[ $SRCFileDate > $OUTFileDate ]]; then
		buildOK="true"
		if [[ $dev == "true" ]]; then
			pandoc --mathjax -f markdown -t html5 -s "$TheFile".md \
			-B includes/header.html -A includes/footer.html \
			-c includes/style.css  --template includes/template-dev.html \
			-o "$SRC"/"$TARGETDIR"/docs/"$TheFileShort".html
		else
			pandoc --mathjax -f markdown -t html5 -s "$TheFile".md \
			-B includes/header.html -A includes/footer.html \
			-c includes/style.css  --template includes/template-site.html \
			-o "$SRC"/"$TARGETDIR"/docs/"$TheFileShort".html
		fi
   fi
   
done

echo "done, syncing ... "

if [[ $buildOK == "true" ]]; then
	rsync -a --update "$SRC"/fonts/ "$SRC"/"$TARGETDIR"/fonts/
	rsync -a --update "$SRC"/includes/ "$SRC"/"$TARGETDIR"/includes/
	rsync -a --update "$SRC"/resources/ "$SRC"/"$TARGETDIR"/resources/
	
	rsync -a --update "$SRC"/"$TARGETDIR"/docs/index.html "$SRC"/"$TARGETDIR"/index.html
	rsync -a --update "$SRC"/"$TARGETDIR"/docs/404.html "$SRC"/"$TARGETDIR"/404.html
	rsync -a --update "$SRC"/docs/feed.rss "$SRC"/"$TARGETDIR"/feed.rss
fi

echo "job completed"
	
logit "STOP" "Version $Version - Mode/Level $MODE/$LEVEL"

