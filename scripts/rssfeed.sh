#!/bin/bash
#
# rssfeed.sh
#
# Script to create rss feed

#
SWNAME="rssfeed"
Version="0.0.0"
 
LEVEL="V"
#

# temp file
tmpTitle=""
tmpDate=""
tmpTags=""

#
# Where am I installed?
# all other relevant dirs are relative to this.
#
DIR=$(cd "$(dirname "$0")" && pwd)
ind=$(echo "$DIR" | grep -b -o "/scripts" | cut -d: -f1)
#ind=$((ind - 9))

SRC=${DIR:0:$ind}

#
# Functions
#
# Functions are in functions.sh, load it ...

. "$SRC"/scripts/functions.sh


#
## "Main" section
#


SYYMMDD=$(date +%Y-%m-%d)

logit "START" "($Version) "

if [[ ! -f "$SRC"/data-sort.tmp ]]; then
	return 99
fi

logit ">post" "Start"

OutFile="$SRC"/docs/feed.rss

# Create new rss file and send new data to it

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>" > "$OutFile"
echo "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">" >> "$OutFile"
echo "<channel>" >> "$OutFile"
echo "<title>~medlock</title>" >> "$OutFile"
echo "<link>https://medlock.envs.net/</link>" >> "$OutFile"
echo "<language>en-gb</language>" >> "$OutFile"
echo "<description>A ~ tilde site</description>" >> "$OutFile"
echo "<atom:link href=\"https://medlock.envs.net/feed.rss\" rel=\"self\" type=\"application/rss+xml\" />" >> "$OutFile"
echo "<item>" >> "$OutFile"

nCnt=0

IFS=','

logit ">post" "Start"

while read -r tmpDate tmpType tmpFile tmpTitle tmpTags
do
    len=${#tmpFile}

    htmlFile=${tmpFile:0:$len-3}

    if [[ $tmpType == "post" ]]; then
        echo "<item>" >> "$OutFile"
        echo "<title>Backgammom</title>" >> "$OutFile"
        echo "<link>https://medlock.envs.net/docs/backgammon.html</link>" >> "$OutFile"
        echo "<guid> https://medlock.envs.net/docs/backgammon.html</guid>" >> "$OutFile"
        echo "<pubDate>Mon, 12 Jan 2022 15:10:45 GMT</pubDate>" >> "$OutFile"
        echo "<description> </description>" >> "$OutFile"
        echo "/<item>" >> "$OutFile"
  
        ((nCnt++))
    
    fi
done < "$SRC"/data-sort.tmp

echo "</channel>" >> "$OutFile"
echo "</rss>" >> "$OutFile"


logit "" "Completed"

logit "STOP!" "Version $Version"
