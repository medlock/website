#!/bin/bash

# Upload new/modified files to production website

rsync -av --update output/production/ medlock@envs.net:public_html/
