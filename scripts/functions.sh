# 
# FU N  C   T    I     O      N       S
#
# C Medlock
#
# April 2020, Version 1.1
#
# A list of common functions ...
#
# Revision History
# ------------------------------------------------------------------------------
# 2020-04  1.0      Initial release
# 2022-01  1.1		Added extra functions
#
# ------------------------------------------------------------------------------

# Function to log activities
logit(){

	if [[ $LEVEL == "V" ]]; then
		TIME=`date +"%Y-%m-%d %H%M%S"`
		echo "$TIME $SWNAME [$1] {$2)" >> $SRC/logs/job.log
	fi
}

# Function to load common run variables
getDefs(){
	
rtnCode="0"

TheServer="Err"
TheBaseURL="Err"
TheSiteName="Err"
TheSiteDesc="Err"
TheSiteLang="Err"
TheSitePublicDir="Err"
TheSiteDocs="Err"


if [[ ! -f $SRC/config.yml ]]; then
		echo "config.yml doesn't exist"
		rtnCode="80"
		return 80
fi

while read inRec
do

	len=`echo ${#inRec}`
	pos=`echo $inRec | grep -b -o ": " | cut -d: -f1`
	
	if [[ $inRec == *"server:"* ]]; then
		TheServer=${inRec:pos+1:$len}
	fi

	if [[ $inRec == *"baseurl:"* ]]; then
		TheBaseURL=${inRec:pos+1:$len}
	fi
	
	if [[ $inRec == *"site:"* ]]; then
		TheSiteName=${inRec:pos+1:$len}
	fi
	
	if [[ $inRec == *"desc:"* ]]; then
		TheSiteDesc=${inRec:pos+1:$len}
	fi
	
	if [[ $inRec == *"lang:"* ]]; then
		TheSiteLang=${inRec:pos+1:$len}
	fi
	
	if [[ $inRec == *"pubdir:"* ]]; then
		TheSitePublicDir=${inRec:pos+1:$len}
	fi
	
	if [[ $inRec == *"artdir:"* ]]; then
		TheSiteDocs=${inRec:pos+1:$len}
	fi	
done < $SRC/config.yml

	if [[ $TheServer == "Err" || $TheBaseURL == "Err" || $TheSiteName == "Err" || $TheSiteDesc == "Err" || $TheSiteLang == "Err" || $TheSitePublicDir == "Err" || $TheSiteDocs == "Err" ]]; then
		echo "Check contents of config.yml. One or more parameters invalid"
		echo $TheServer
		echo $TheBaseURL
		echo $TheSiteName
		echo $TheSiteDesc
		echo TheSiteLang
		echo $TheSitePublicDir
		echo $TheSiteDocs
		rtnCode="99"
		return 99
	fi
}

# Function to get length of a string and position of given char in the string
# Two parms, the string, and what to look for:-
#
# eg LenWhere $TheString ":"
#
#		"ABC: test1" -> would give len=10 and pos=3
#

LenWhere()
{
	len=${#$1}																	# length of string	
	pos=$(echo "$S1" | grep -b -o "$S2 " | cut -d: -f1)							# Where is $S2 in string
}


WhereAmI()
{
#
# Where am I installed?
# all other relevant dirs are relative to this.
#
DIR=$(cd "$(dirname "$0")" && pwd)
ind=$(echo "$DIR" | grep -b -o "/scripts" | cut -d: -f1)
#ind=$((ind - 9))

SRC=${DIR:0:$ind}

}
