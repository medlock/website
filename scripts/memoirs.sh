#!/bin/bash
#
# memoirs.sh
#
# Script to create entries in
#  posts.md
#  index.md
#  memoirs.md

# ALPHA - proof of concept!
#   
# Need to 
#   1) make more effiecient and 
#   2) Define functions to reduce repeated code!
#   3) Remove hardcode stuff        
#   4) Make more robust!
#
# -- Constants --
#

MODE="D"                                                                        # Production or Debug                                                                      # Debug
LEVEL="V"
#
SWNAME="memoirs"
Version="0.0.0"
#

#
#
# Where am I installed?
# all other relevant dirs are relative to this.
#
DIR=$(cd "$(dirname "$0")" && pwd)
ind=$(echo "$DIR" | grep -b -o "/scripts" | cut -d: -f1)
#ind=$((ind - 9))

SRC=${DIR:0:$ind}

#
# Functions
#
# Functions are in functions.sh, load it ...

. "$SRC"/scripts/functions.sh

#
# Update memoirs.md 
# 


SYYMMDD=$(date +%Y-%m-%d)

OutFile="$SRC"/docs/memoirs.md

#nCnt=0

IFS=','

logit "memoirs" "Start"

rm -f "$OutFile"

AutoCmnt="false"


while read -r tmpDate tmpType tmpFile tmpTitle tmpTags
do
    len=${#tmpFile}

    htmlFile=${tmpFile:0:$len-3}

    if [[ $tmpType == "post" && $tmpTags == *"memoirs"* ]]; then
		if [[ $tmpDate < $SYYMMDD || $tmpDate == $SYYMMDD ]]; then									# check to see if new
			if [[ $AutoCmnt == "false" ]]; then									# memoir to list
				while read -r infile
				do
					if [[ $infile == *"Modified "* ]]; then
						len=${#infile}
						tmpRec=${infile:0:len-11}
						echo "$tmpRec $tmpDate  " >> "$OutFile"
					else 
						echo "$infile" >> "$OutFile"
					fi

					if [[ $infile == *"---"* && $AutoCmnt == "false" ]]; then
						echo "comment: AUTO GENERATED! - Edit template version of this file." >> "$OutFile"
						AutoCmnt="true"
					fi
				done < "$SRC"/templates/memoirs.md
			fi
			echo "  - [$tmpTitle ($tmpDate)](/docs/$htmlFile.html)" >> "$OutFile"
        fi
    fi

done < "$SRC"/data-sort.tmp

logit "memoirs" "Completed"

logit "STOP!" "Version $Version - Mode/Level $MODE/$LEVEL"
