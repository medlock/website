[![Build Status](https://drone.envs.net/api/badges/medlock/website/status.svg)](https://drone.envs.net/medlock/website)

A website built with [Pandoc](https://pandoc.org).

Inspired by [TSPW](https://github.com/eakbas/TSPW).


**To Do**

Gitea/Drone

- ~~Modify drone to run Pandoc locally on target server~~ Done, Jan 2022

- Need to make drone just copy updated files 

	Think this is an issue with git and dates because the clone in drone 
	rsync is correct but still copies everything.)

Website

- tags / categories
- bash script to generate output
- photo gallery
- RSS feed / sitemap
-  ~~Site to use local fonts (prevents tracking)~~ Done, Jan 2022


