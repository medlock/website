---
comment: AUTO GENERATED! - Edit template version of this file.
title: Memoirs
type: page
ref: Published 2020-09-18, Modified 2022-01-19  
---

Having spent a long time in IT and I have some stories to tell. 

On this page I will share (mostly funny / good-natured) anecdotes from the IT world. 

On occasion, I might use this space to have a whinge. **You have been warned**.

I was lucky enough to work in profession that I thoroughly enjoyed, to me it wasn't work but fun. Most of the time it was great, with a good bunch of people.  Did some great, interesting work that I'm extremely proud of too. 

I have also made some silly mistakes on my IT journey too. Such is life! 

All will be revealed, in good-time.

[more ...](/docs/memoirs-2.html)
<br>

Recent Posts

  - [Memoirs - There are Programmers ... (2022-01-19)](/docs/memiors-programmers.html)
  - [Memoirs - School Boy Error (2020-09-18)](/docs/memoirs-SchoolBoyError.html)
  - [Memoirs - Wang Car (2017-06-03)](/docs/memoirs-Wang-Car.html)
  - [Memoirs - I Use 4 Screens (2017-05-17)](/docs/memoirs-I-Use-4-Screens.html)
