---
title: Memoirs - School Boy Error
ref: Published 2020-09-18
type: post
tags: memoirs, error
---

Time to mock my own stupidity.

This anecdote comes from my time working for an ISP.

The job was to set-up a home router for a customer. I was not entirely familiar with the kit (but this is not an excuse).

I could not get the damn thing to work properly. Rebooted the device, reloaded the config a few times and I still couldn't see the problem. Revisited the settings - IP address, sub-mask, gateway, etc, all the usual stuff.

Anyway, after spending all of 30 mins (actually much longer), I gave up and asked for help.

To my embarrassment, and I mean my absolute shame, my boss spotted the error - almost immediately.

The problem? Well I'd only given it the wrong gateway address.

To which my boss, justifiably commented that it was a "Schoolboy error ...".

And, you know what, I agree with him, it was.


To this day, I'm always extra careful about the g/w and it is the first thing I check when there are connectivity issues, I always remember this incident with a chuckle. 

I'm pretty sure my boss would have a laugh about it now too! 
