---
title: The Pale Blue Dot
type: post
ref: Published 2022-02-14, Modified 2022-02-16 (formatting)
tags: voyager pale_blue_dot
---
Today makes another Voyager 1 anniversary.


<img src="/resources/TPBD/PaleBlueDot.jpg" id="fig_pic_right"/>

<br>
On the 14th February 1990, Voyager 1 was about to leave the solar system - some 6,000 million kilometres away. 

It was instructed to look back and take a picture of our neighbourhood. 

The result is this haunting photo of Earth, a “pale blue dot” - a mere 0.12 of a pixel in size. 

<br>
**OUR home**.

<br>
<br>
On the imaging team responsible for this picture was famed planetary scientist [Dr Carl Sagan](https://en.wikipedia.org/wiki/Carl_Sagan).


He would later write ... 

<p class="Abst">
"From this distant vantage point, the Earth might not seem of any particular interest. But for us, it's different. Consider again that dot. That's here. That's home. That's us.
<p>
<p class="Abst">
On it everyone you love, everyone you know, everyone you ever heard of, every human being who ever was, lived out their lives.
<p>
<p class="Abst">
The aggregate of our joy and suffering, thousands of confident religions, ideologies, and economic doctrines, every hunter and forager, every hero and coward, every creator and destroyer of civilization, every king and peasant, every young couple in love, every mother and father, hopeful child, inventor and explorer, every teacher of morals, every corrupt politician, every 'superstar,' every 'supreme leader,' every saint and sinner in the history of our species lived there - on a mote of dust suspended in a sunbeam.
<p>
<p class="Abst">
The Earth is a very small stage in a vast cosmic arena. Think of the rivers of blood spilled by all those generals and emperors so that in glory and triumph they could become the momentary masters of a fraction of a dot. Think of the endless cruelties visited by the inhabitants of one corner of this pixel on the scarcely distinguishable inhabitants of some other corner.
<p>
<p class="Abst">
How frequent their misunderstandings, how eager they are to kill one another, how fervent their hatreds. Our posturings, our imagined self-importance, the delusion that we have some privileged position in the universe, are challenged by this point of pale light.
<p>
<p class="Abst">
Our planet is a lonely speck in the great enveloping cosmic dark.
<p>
<p class="Abst">
In our obscurity - in all this vastness - there is no hint that help will come from elsewhere to save us from ourselves.
<p>
<p class="Abst">
The Earth is the only world known, so far, to harbor life. There is nowhere else, at least in the near future, to which our species could migrate.
<p>
<p class="Abst">
Visit, yes. Settle, not yet. Like it or not, for the moment, the Earth is where we make our stand. It has been said that astronomy is a humbling and character-building experience. There is perhaps no better demonstration of the folly of human conceits than this distant image of our tiny world.
<p>
<p class="Abst">
To me, it underscores our responsibility to deal more kindly with one another and to preserve and cherish the pale blue dot, the only home we've ever known."
</p>

<br>

<img src="/resources/TPBD/CarlSagan.jpg" id="fig_pic_right"/>

Carl Edward Sagan (/ˈseɪɡən/; 9th November 1934 - 20th December 1996 (62) was an astronomer, cosmologist, astrophysicist, astrobiologist, author, and science communicator.

He was a member of the Voyager imaging team and produced a series of TV programmes to communicate science. I remember his programmes and books. His wonderful voice, and his fantastic use of words in his explanation of science. Wonderful, magical days.

He died of cancer in 1996.

<br>
Carl Sagan, 1980

<br>


<img src="/resources/TPBD/View_Voyager_1.png" id="fig_pic_left"/>

<br>

Original photo taken by Voyager 1, with the sun on the right, and processed regions (left - Earth, right - Venus). 

The “sunbeams” are artefacts of the imaging process. 


<br>
<br>
<br>
<br>
<br>
<br>
<br>


<img src="/resources/TPBD/Voyager_1_Feb-1990.png" id="fig_pic_right"/>
<br>
<br>
<br>

Voyager 1 location on the 14th February 1990


Launched 5th September 1977

<br>
<br>
<br>
<br>
<br>
<br>


**So, where is Voyager 1 now?** [nasa:voyager:status](https://voyager.jpl.nasa.gov/mission/status/)

Scroll almost to the bottom of this page and there is an interactive map.

<br>
**Closing Thoughts**

When I read or better still [listen to](/resources/TPBD/PaleBlueDot.mp4) what Carl Sagan said all those years ago, it always makes me feel incredibly sad.

He had tremendous foresight; yet, once again, as a species, we haven't learnt anything!!!!

If you aren't shocked, or "challenged" as Carl Sagan put it, when you gaze at the first photo, well what's up with you? 

**Look at it.** Would you have found Earth if I'd not put a ring around her?

Don't you feel small, may be lonely, possibly afraid?

It should put things into perspective. I hope it does!

We should look after the ONLY home we have (and to most, if not all - the **ONLY home** we will ever have)! 

Yet we treat it like dirt, polluting it, destroying habitat, condemning life to **avoidable** hardship. 

Powerful companies are allowed to rape and pillage the Earth, in the name of profit.

Earth is like a tiny spacecraft, where its inhabitants insist in sabotaging the life support systems. 

How do you think that will end for us if we persist? Not good, I can assure you!

**Insanity personified**.

I do wonder what an alien civilisation would make of it. Would it class us as parasites? Totally undeserving of this **wonderful place** we have inherited?

We certainly wouldn't have a leg to stand on!

**We should be preserving & fostering life!**

**We should look upon ourselves and act like custodians.** 

Not like spoilt little brats, basically doing as we please. 

**We don't have that right**.

Earth is our mother. She gave us life, she looks after us, she supports us, sometimes scolds us too.

But if we carry on our current path she will give us one almighty spanking.

**She will survive, we will not.**
