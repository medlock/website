---
title: Can Dark Energy be Ditched?
type: post
ref: Published 2017-07-14
tags: physics, cosmology, dark-energy
---

<h1>Can we ditch dark energy by better understanding general relativity?</h1>

  <figure>
    <img src="https://cdn.theconversation.com/files/176050/width754/file-20170628-7294-1ssrxrf.png" alt="File 20170628 7294 1ssrxrf" />
      <figcaption>
        Simulated universe: EAGLE collaboration, J Schaye et al 2015.
        <span class="attribution"><a class="source" href="https://doi.org/10.1093/mnras/stu2058">MNRAS</a>, <a class="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a></span>
      </figcaption>
  </figure>

<span><a href="https://theconversation.com/profiles/david-wiltshire-362828">David Wiltshire</a> and <a href="https://theconversation.com/profiles/alan-coley-367913">Alan Coley</a>, <em><a href="http://theconversation.com/institutions/dalhousie-university-1329">Dalhousie University</a></em></span>

<p><a href="http://www.sciencemag.org/news/2017/04/dark-energy-illusion">A renewed suggestion that dark energy may not be real</a> — dispensing with 70% of the stuff in the universe — has <a href="https://telescoper.wordpress.com/2017/03/28/is-there-a-kinematic-backreaction-in-cosmology/">reignited a longstanding debate</a>.</p>

<p>Dark energy and dark matter are theoretical inventions that explain observations we cannot otherwise understand.</p>

<p>On the scale of galaxies, gravity appears to be stronger than we can account for using only particles that are able to emit light. So we add dark matter particles as 25% of the mass-energy of the Universe. Such particles have never been directly detected.</p>

<p>On the larger scales on which the Universe is expanding, gravity appears weaker than expected in a universe containing only particles – whether ordinary or dark matter. So we add “dark energy”: a weak anti-gravity force that acts independently of matter.</p>

<h2>Brief history of “dark energy”</h2>

<p>The idea of dark energy is as old as general relativity itself. Albert Einstein included it when he first applied relativity to cosmology exactly 100 years ago.</p>

<p>Einstein mistakenly wanted to exactly balance the self attraction of matter by anti-gravity on the largest scales. He could not imagine that the Universe had a beginning and did not want it to change in time.</p>

<p>Almost nothing was known about the Universe in 1917. The very idea that galaxies were objects at vast distances was debated.</p>

<p>Einstein faced a dilemma. The physical essence of his theory, as summarised decades later in the <a href="https://web.archive.org/web/20160202083527/http://www.sky-watch.com/books/misner1.html">introduction of a famous textbook</a> is:</p>

<blockquote>
<p>Matter tells space how to curve, and space tells matter how to move.</p>
</blockquote>

<p>That means space naturally wants to expand or contract, bending together with the matter. It never stands still.</p>



<p>This was realised by <a href="http://www.physicsoftheuniverse.com/scientists_friedmann.html">Alexander Friedmann</a> who in 1922 kept the same ingredients as Einstein. But he did not try to balance the amount of matter and dark energy. That suggested a model in which universes that could expand or contract.</p>

<p>Further, the expansion would always slow down if only matter was present. But it could speed up if anti-gravitating dark energy was included.</p>

<p>Since the late 1990s many independent observations have seemed to demand such accelerating expansion, in a Universe with 70% dark energy. But this conclusion is based on the old model of expansion that has not changed since the 1920s.</p>

<h2>Standard cosmological model</h2>

<p>Einstein’s equations are fiendishly difficult. And not simply because there are more of them than in Isaac Newton’s theory of gravity.</p>

<p>Unfortunately, Einstein left some basic questions unanswered. These include – on what scales does matter tell space how to curve? What is the largest object that moves as an individual particle in response? And what is the correct picture on other scales?</p>

<p>These issues are conveniently avoided by the 100-year old approximation — introduced by Einstein and Friedmann — that, on average, the Universe expands uniformly. Just as if all cosmic structures could be put through a blender to make a featureless soup.</p>

<p>This homogenising approximation was justified early in cosmic history. We know from the <a href="https://theconversation.com/the-cmb-how-an-accidental-discovery-became-the-key-to-understanding-the-universe-45126">cosmic microwave background</a> — the relic radiation of the Big Bang — that variations in matter density were tiny when the Universe was less than a million years old.</p>

<p>But the universe is <em>not</em> homogeneous today. Gravitational instability led to the growth of stars, galaxies, clusters of galaxies, and eventually a vast “<a href="https://blogs.scientificamerican.com/sa-visual/the-beautiful-complexity-of-the-cosmic-web/">cosmic web</a>”, dominated in volume by voids surrounded by sheets of galaxies and threaded by wispy filaments.</p>

<p>In standard cosmology, we assume a background expanding as if there were no cosmic structures. We then do computer simulations using only Newton’s 330-year old theory. This produces a structure resembling the observed cosmic web in a reasonably compelling fashion. But it requires including dark energy and dark matter as ingredients.</p>

<p>Even after inventing 95% of the energy density of the universe to make things work, the model itself still faces <a href="https://doi.org/10.1142/S021827181630007X">problems that range from tensions to anomalies</a>.</p>

<p>Further, standard cosmology also fixes the curvature of space to be uniform everywhere, and decoupled from matter. But that’s at odds with Einstein’s basic idea that matter tells space how to curve.</p>

<p>We are not using all of general relativity! The standard model is better summarised as: <em>Friedmann tells space how to curve, and Newton tells matter how to move.</em></p>

<h2>Enter “backreaction”</h2>

<p>Since the early 2000s, some cosmologists have been exploring the idea that while Einstein’s equations link matter and curvature on small scales, <a href="https://cqgplus.com/2016/01/20/the-universe-is-inhomogeneous-does-it-matter/">their large-scale average might give rise to backreaction</a> – average expansion that’s not exactly homogeneous.</p>

<p>Matter and curvature distributions start out near uniform when the universe is young. But as the cosmic web emerges and becomes more complex, the variations of small-scale curvature grow large and average expansion can differ from that of standard cosmology.</p>

<p>Recent numerical results of a team in Budapest and Hawaii that <a href="https://www.ras.org.uk/news-and-press/2968-explaining-the-accelerating-expansion-of-the-universe-without-dark-energy">claim to dispense with dark energy</a> used standard Newtonian simulations. But they evolved their code forward in time by a non-standard method to model the backreaction effect.</p>

<p>Intriguingly, the resulting expansion law fit to Planck satellite data tracks very close to that of a <a href="http://www.abc.net.au/science/articles/2007/12/21/2124258.htm?site=science&amp;topic=space">ten-year-old general relativity-based backreaction model</a>, known as the <a href="http://iopscience.iop.org/article/10.1088/1367-2630/9/10/377/meta">timescape cosmology</a>. It posits that we have to calibrate clocks and rulers differently when considering variations of curvature between galaxies and voids. For one thing, this means that <a href="http://dx.doi.org/10.1142/S0218271809016193">the Universe no longer has a single age</a>.</p>

<p>In the next decade, experiments such as the <a href="https://www.euclid-ec.org/">Euclid satellite</a> and the <a href="http://www.iac.es/proyecto/codex/">CODEX experiment</a>, will have the power to test whether cosmic expansion follows the homogeneous law of Friedmann, or an alternative backreaction model.</p>

<figure class="align-center ">
            <img alt="" src="https://cdn.theconversation.com/files/175918/width754/file-20170627-24813-eonxfz.jpg">
            <figcaption>
              <span class="caption">An artist’s impression shows the European Extremely Large Telescope (E-ELT) which uses CODEX as an optical, very stable, high spectral resolution instrument.</span>
              <span class="attribution"><a class="source" href="https://www.flickr.com/photos/esoastronomy/15761494008/in/photolist-q1MM3G-U3wv4B-TZEDSw-p4AoBH-GDdQb6-qzQhJL-vdGXRR-V5tcSX-RCqRAt-8cyJwa-ebwsGL-GJya5o-SHfYDC-mx6k24-ebycf7-eJauSe-dgjFKN-gvJK2g-RfwAu7-8S7s3b-SByBgd-qazbaA-kj2hBv-ouPNaj-oiVa2N-qMoqtL-ohRgHf-ohd8qk-ogK5gA-o1ZoFm-eoYhnV-garxQW-ohXWkn-rpssjK-kJGgA9-qRRYJN-ohUZgG-ogPnxo-rpzCHa-bR9WLa-nY57gS-bimc4x-oXodS3-FGXhQh-8c5abM-8S4K6V-8fWZPs-8c8Biq-o2b7DP-W1EhY6">ESO/L. Calçada</a>, <a class="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a></span>
            </figcaption>
          </figure>

<p>To be prepared, it’s important that we don’t put all our eggs in one cosmological basket, as <a href="http://www.nature.com/news/good-data-are-not-enough-1.20906">Avi Loeb, Chair of Astronomy at Harvard, has recently warned</a>. In Loeb’s words:</p>

<blockquote>
<p>To avoid stagnation and nurture a vibrant scientific culture, a research frontier should always maintain at least two ways of interpreting data so that new experiments will aim to select the correct one. A healthy dialogue between different points of view should be fostered through conferences that discuss conceptual issues and not just experimental results and phenomenology, as often is the case currently.</p>
</blockquote>

<h2>What can general relativity teach us?</h2>

<p>While most researchers accept that the backreaction effects exist, the real debate is about whether this can lead to more than a 1% or 2% difference from the mass-energy budget of standard cosmology.</p>

<p>Any backreaction solution that eliminates dark energy must explain why the law of average expansion appears so uniform despite the inhomogeneity of the cosmic web, something standard cosmology assumes without explanation.</p>

<p>Since Einstein’s equations can in principle make space expand in extremely complicated ways, some simplifying principle is required for their large-scale average. This is <a href="https://doi.org/10.1103/PhysRevD.78.084032">the approach of the timescape cosmology</a>.</p>

<p>Any simplifying principle for cosmological averages is likely to have its origins in the very early Universe, given it was much simpler than the Universe today. For the past 38 years, <a href="http://www.ctc.cam.ac.uk/outreach/origins/inflation_zero.php">inflationary universe models</a> have been invoked to explain the simplicity of the early Universe.</p>

<p>While successful in some aspects, <a href="http://iopscience.iop.org/journal/0264-9381/page/Focus-issue-on-Planck-and-fundamentals-of-cosmology">many models of inflation are now ruled out by Planck satellite data</a>. Those that survive give tantalising hints of deeper physical principles.</p>

<p>Many physicists still view the Universe as a fixed continuum that comes into existence independently of the matter fields that live in it. But, in the spirit of relativity – that space and time only have meaning when they are relational – we may need to rethink basic ideas.</p>

<p>Since time itself is only measured by particles with a non-zero rest mass, maybe spacetime as we know it only emerges as the first massive particles condense.</p>

<p>Whatever the final theory, it will likely embody the key innovation of general relativity, namely the dynamical coupling of matter and geometry, at the quantum level.</p>

<p><img src="https://counter.theconversation.edu.au/content/76777/count.gif?distributor=republish-lightbox-basic" alt="The Conversation" width="1" height="1" /><em>Our recently published essay: <a href="https://doi.org/10.1088/1402-4896/aa6857"><em>What is General Relativity?</em></a> further explores these ideas.</em></p>

<p><span><a href="https://theconversation.com/profiles/david-wiltshire-362828">David Wiltshire</a>, Professor of Theoretical Physics and <a href="https://theconversation.com/profiles/alan-coley-367913">Alan Coley</a>, , <em><a href="http://theconversation.com/institutions/dalhousie-university-1329">Dalhousie University</a></em></span></p>

<p>This article was originally published on <a href="http://theconversation.com">The Conversation</a>. Read the <a href="https://theconversation.com/can-we-ditch-dark-energy-by-better-understanding-general-relativity-76777">original article</a>.</p>
