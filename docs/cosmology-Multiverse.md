---
title: CMB Cold Spot - Another Universe?
type: page
ref: Published 2017-06-09
tags: physics cosmology, CMB, multiverse
---

<h1>Could cold spot in the sky be a bruise from a collision with a parallel universe?</h1>

  <figure>
    <img src="https://cdn.theconversation.com/files/171609/width754/file-20170531-25689-nuzao4.jpg" alt="File 20170531 25689 nuzao4" />
      <figcaption>
        A change in the density of galaxies can’t explain a cold spot in the sky.
        <span class="attribution"><span class="source">NASA and the European Space Agency. Edited by Noodle snacks</span></span>
      </figcaption>
  </figure>

<span><a href="https://theconversation.com/profiles/ivan-baldry-380942">Ivan Baldry</a>, <em><a href="http://theconversation.com/institutions/liverpool-john-moores-university-1319">Liverpool John Moores University</a></em></span>

<p>Scientists have long tried to explain the origin of a mysterious, large and anomalously cold region of the sky. In 2015, <a href="https://theconversation.com/enormous-hole-in-the-universe-may-not-be-the-only-one-40510">they came close to figuring it out</a> as a study showed it to be a “supervoid” in which the density of galaxies is much lower than it is in the rest of the universe. However, other studies haven’t managed to replicate the result.</p>

<p>Now new research led by Durham University, <a href="https://arxiv.org/abs/1704.03814">submitted for publication in the Monthly Notices of the Royal Astronomical Society</a>, suggests the supervoid theory doesn’t hold up. Intriguingly, that leaves open a pretty wild possibility – the cold spot might be the evidence of a collision with a parallel universe. But before you get too excited, let’s look at how likely that would actually be.</p>

<p>The cold spot can be seen in maps of the “cosmic microwave background” (CMB), which is the radiation left over from the birth of the universe. The CMB is like a photograph of what the universe looked like when it was 380,000 years old and had a temperature of 3,000 degrees Kelvin. What we find is that it is very smooth with temperature deviations of less than one part in 10,000. These deviations can be explained pretty well by our models of how the hot universe evolved up to an age of 380,000 years. </p>

<figure class="align-center ">
            <img alt="" src="https://cdn.theconversation.com/files/171626/width754/file-20170531-23531-1ygxhhx.jpg">
            <figcaption>
              <span class="caption">CMB as observed by Planck.</span>
              <span class="attribution"><span class="source">ESA and the Planck Collaboration</span>, <a class="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a></span>
            </figcaption>
          </figure>

<p>However the cold spot is harder to work out. It is an area of the sky about five degrees across that is colder by one part in 18,000. This is readily expected for some areas covering about one degree – but not five. The CMB should look much smoother on such large scales. </p>

<h2>The power of galaxy data</h2>

<p>So what caused it? There are two main possibilities. One is that it could be caused by a supervoid that the light has travelled through. But it could also be a genuine cold region from the early universe. The authors of the new research tried to find out by comparing new data on galaxies around the cold spot with data from a different region of the sky. The new data was obtained by the <a href="https://www.aao.gov.au/about-us/anglo-australian-telescope">Anglo-Australian Telescope</a>, the other by the <a href="http://www.gama-survey.org">GAMA survey</a>. </p>

<p>The GAMA survey, and other surveys like it, take the “spectra” of thousands of galaxies. Spectra are images of light captured from a galaxy and spread out according to its wavelengths. This provides a pattern of lines emitted by the different elements in the galaxy. The further away the galaxy is, the more the expansion of the universe shifts these lines to appear at longer wavelengths than they would appear on Earth. The size of this so-called “redshift” therefore gives the distance to the galaxy. Spectra coupled with positions on the sky can give us 3D maps of galaxy distributions. </p>

<p>But the researchers concluded that there simply isn’t a large enough void of galaxies to explain the cold spot – there was nothing too special about the galaxy distribution in front of the cold spot compared to elsewhere. </p>

<p>So if the cold spot is not caused by a supervoid, it must be that there was a genuinely large cold region that the CMB light came from. But what could that be? One of the more exotic explanations is that there was a collision between universes in a very early phase. </p>

<h2>Controversial interpretation</h2>

<p>The idea that we live in a “multiverse” made up of an infinite number of parallel universes has long been considered a <a href="https://theconversation.com/the-theory-of-parallel-universes-is-not-just-maths-it-is-science-that-can-be-tested-46497">possibility</a>. But physicists still disagree about whether it could represent a physical reality or whether it’s just a mathematical quirk. It is a consequence of important theories like quantum mechanics, string theory and inflation.</p>

<p>Quantum mechanics oddly states that any particle can exist in “superposition” – which means it can be in many different states simultaneously (such as locations). This sounds bizarre but it has been observed in laboratories. For example, electrons can travel through two slits at the same time – when we are not watching. But the minute we observe each slit to catch this behaviour, the particle chooses just one. That is why, in the famous <a href="http://news.nationalgeographic.com/news/2013/08/130812-physics-schrodinger-erwin-google-doodle-cat-paradox-science/">“Shroedinger’s cat” thought experiment</a>, an animal can be alive and dead at the same time. </p>

<p>But how can we live with such strange implications? One way to interpret it is to choose to accept that all possibilities are true, but that they exist in different universes.</p>

<figure class="align-center ">
            <img alt="" src="https://cdn.theconversation.com/files/93451/width754/image-20150831-25771-t0149q.jpg">
            <figcaption>
              <span class="caption">Miaowtiverse.</span>
              <span class="attribution"><a class="source" href="https://www.flickr.com/photos/29233640@N07/8132455446">Robert Couse-Baker/Flickr</a>, <a class="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a></span>
            </figcaption>
          </figure>

<p>So, if there is mathematical backing for the existence of parallel universes, is it so crazy to think that the cold spot is an imprint of a colliding universe? Actually, it is extremely unlikely.  </p>

<p>There is no particular reason why we should just now be seeing the imprint of a colliding universe. From what we know about how the universe formed so far, it seems likely that it is much larger than what we can observe. So even if there are parallel universes and we had collided with one of them – unlikely in itself – the chances that we’d be able to see it in the part of the universe that we happen to be able to observe on the sky are staggeringly small.  </p>

<p>The <a href="https://arxiv.org/abs/1704.03814">paper</a> also notes that a cold region of this size could occur by chance within our standard model of cosmology – with a 1%-2% likelihood. While that does make it unlikely, too, it is based on a model that has been well tested so we cannot rule it out just yet. Another potential explanation is in the <a href="https://ned.ipac.caltech.edu/level5/March01/Coles/Coles2.html">natural fluctuations in mass density</a> which give rise to the CMB temperature fluctuations. We know these exist on all scales but they tend to get smaller toward large scales, which means they may not be able to create a cold region as big as the cold spot. But this may simply mean that we have to rethink how such fluctuations are created. </p>

<p><img src="https://counter.theconversation.edu.au/content/78563/count.gif?distributor=republish-lightbox-basic" alt="The Conversation" width="1" height="1" />It seems that the cold spot in the sky will continue to be a mystery for some time. Although many of the explanations out there seem unlikely, we don’t necessarily have to dismiss them as pure fantasy. And even if it takes time to find out, we should still revel in how far cosmology has come in the last 20 years. There’s now a detailed theory explaining, for the most part, the glorious temperature maps of the CMB and the cosmic web of galaxies which span across billions of light years.</p>

<p><span><a href="https://theconversation.com/profiles/ivan-baldry-380942">Ivan Baldry</a>, Professor of Astrophysics, <em><a href="http://theconversation.com/institutions/liverpool-john-moores-university-1319">Liverpool John Moores University</a></em></span></p>

<p>This article was originally published on <a href="http://theconversation.com">The Conversation</a>. Read the <a href="https://theconversation.com/could-cold-spot-in-the-sky-be-a-bruise-from-a-collision-with-a-parallel-universe-78563">original article</a>.</p>
