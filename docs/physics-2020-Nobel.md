---
title: The Nobel Prize in Physics 2020 - Roger Penrose
type: post
ref: Published 2021-10-15
tags: physics, Penrose, Nobel
---

Great news to hear that Sir Roger Penrose has won the Nobel Prize in Physics!

**Congratulations Sir Roger! - A much deserved recipient.**

He has had to wait a long time, the work that won the prize was written in 1963!

One can read about it [here](https://astrobites.org/2020/11/23/conformal-diagram/).

It also introduces [Penrose Diagrams](https://en.wikipedia.org/wiki/Penrose_diagram), a tool that physicists use a lot now.

Sir Roger is one of my favourite theorists. 

I first noticed his work during the many BBC [Horizon](https://www.imdb.com/title/tt0318224/) programmes ('70's & '80's) on science, broadcast in the UK.  He also took part in [Einstein's Universe](https://www.imdb.com/title/tt6061610/) (1979), a programme about a book baring the same title to celebrate the centenary of Einstein's birth.

I could listen to him for hours and never get bored.

[Sir Roger](https://en.wikipedia.org/wiki/Roger_Penrose) is famous for many other contributions to maths and physics. He is also quite a good artist. He famously still uses acetate slides in his lectures.

Nowadays, he is sometimes regarded as a bit of a crank by some physicists. Which is a tad disrespectful and unwarranted. He always treats this with good humour, jokingly referring to it due to is age (he is in his 90's). However, he is never afraid to ask difficult questions (particularly the ones that physicists like to ignore), and has a truly furtive mind.

We need people like Sir Roger to push the boundaries to see what is and isn't possible. That is part of what science is about. 

Einstein famously said that "imagination is more important than knowledge". 

Some physicists seem to have forgotten that!

His latest controversies relate to:-

*  [Conformal Cyclic Cosmology (CCC)](https://en.wikipedia.org/wiki/Conformal_cyclic_cosmology) - where conformal geometry is used to describe a cyclic universe.
*  [Consciousness](https://www-physics.lbl.gov/~stapp/PenHam.pdf) - where quantum mechanic describes conscious.

He is a giant in mathematical physics, let's hope he continues his wonderful work for some time to come!
