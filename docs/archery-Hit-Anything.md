---
title: Yer wern't 'it anything wi' that!
type: post
ref: Published 2018-05-02
tags: archery, 252, longbow, Yorkshire
---

"Yer wern't 'it anything wi' that!" shouted a fellow archer in his thick broad Yorkshire accent.

He was, of course, referring to my longbow - a primitive "stick" compared to his brand new shiny Hoyt recurve bow - complete with all the gizmos. 

There was a hint of snideness in his voice. 

**There was only one answer, "educate" him just how good a longbow could be!**

I was about to do [252](/docs/archery-252.html). For the longbow, the target score is actually 189, as it is generally acknowledged that the longbow is simply not as accurate as a recurve bow.

That day, due to the taunt, I decided that the target score was 252 or higher and I proceeded to post 289 (out of 324). But what pleased me the most was my grouping, I was on fire!

I went on to demonstrate just how good a longbow could be. The "miserable old git" was suitably unimpressed, but he never said that again. So my point must have hit home (excuse the pun).


So, that's why I always use 252 as the target score, just to prove a point for those who look down on the longbow archer. 

Besides, you can bet your house, if I'd taken the 189 score, his only comment would be "well, you were helped, blah, blah ...".

It rather reminds me of when I used to compete in aerobatics, and some of my competitors turning up in Pitts Specials, or Extras. Vastly superior aerobatic planes to my lowly Slingsby T67m.

We were, after all, only in the **beginners class**, for goodness sake! 

**Some people will do anything to gain the upper hand.**

Another story.