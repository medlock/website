---
title: The Shot Cycle
type: post
ref: Published 2017-07-01
tags: archery, technique
---

These are my notes for the great series of posts by 

[Archery 101 - The Shot Cycle.](https://www.youtube.com/watch?v=SyrKM4MZdEE&list=PL39zBcqaWKsqa_Fp7YIM7Sp9dAmg2Ntuc)

<iframe width="480" height="360" src="https://www.youtube.com/embed/SyrKM4MZdEE" frameborder="0"> </iframe>


<p> </p>
***Repeatable consistent actions are key to being a good archer.***

<p> </p>
<p> </p>
## 1) The Stance

***The foundation of a good shot cycle***

- Tension is your enemy - relax!
- Chest compression - keep ribs  down -> lower Centre of gravity
- Weight distribution - 70% on balls of feet

Stances - squared, open, oblique, closed, narrow (don't touch the shooting line)

<p> </p>
<p> </p>
## 2) Nock Arrow

Don't infringe neighbours space.

<p> </p>
<p> </p>
## 3) Hook String
<p> </p>
- Cleaner release
- Consistent anchor
- Prevent injuries

Weight distribution -> hook - index finger, full draw - middle finger

Thumb goes down and touches little finger - this will go on to form one of the anchor points.

<p> </p>
<p> </p>
## 4) Set Grip

Bow dictates grip to use, solid grip - better shot, accuracy.

Grip should be natural, comfortable, and easily repeatable.

Pivot Point - web of hand.
Pressure Point - meaty part of thumb (right of centre rhd, left of centre for lhd).

Steeper handle, closer two points are together.

- Light grip no squeezing
- Relaxed and natural
- Thumb pointing at target


***Set pivot point first then pressure point.***

<p> </p>
<p> </p>
## 5) Set-up

Focus the mind, what do you need to do?
Where do you want to hit ? Focus on that point stay focused.
Position bow prior to draw.
Set shoulder - back and down (pull shoulder in then down whilst still pushing forward with hand).

<p> </p>
<p> </p>
## 6) Draw

Highly repetitive movement- injuries if not done right.

- Linear - most likely to cause injury - pull back using hand
- J-motion - uses back, pull elbow back
- Rotation - uses body rotate the body

Motion natural, smooth, and straight to anchor

<p> </p>
<p> </p>
## 7) Anchor(s)

More anchor points more consistency!

Traditional - corner of mouth, thumb/little finger touching neck, (feather to nose).

Recurve - Under the chin, string off-centre of mouth, at side. String touching nose. Thumb/little finger touching neck - to neck, for anchor points.

Drawing arm should form a straight line with the arrow.

<p> </p>
<p> </p>
## 8) Transfer

(part of the draw process)

Transfer pressure of holding bow at full draw  from upper back to lower back.

- Less fatigue
- Consistency
- More strength plan

Arrow and drawing arm form a straight line.
Minimise plucking.
Don't lower elbow and shoulder, both shoulders come down  - "like" the Hulk.
Squeeze down and in.
No tension around neck.

Where effort to hold bow, not in front?

***REMEMBER - not feel right - come down and start again***

Slow down the draw near end the to incorporate this if you shoot quickly.

<p> </p>
<p> </p>
## 9) Aim

Focus on point of target.

Don't move your eyes, hand will follow your eyes!

Mantra - keep pulling, keep pulling ...

<p> </p>
<p> </p>
## 10) Release

Dynamic or static release

- Dynamic - hand goes back as a result of back tension
- Static - hand does not move back

Poor release is rarely that - it is the result of variation of the preceding steps.

Primary reasons for bad shot

- Plucking - normally caused by the drawing arm not in alignment with arrow.
- Creeping - inadequate back tension.
- Assisted release - lack of control of the shot cycle.

<p> </p>
<p> </p>
## 11) Follow Through

Hold position and make a mental note of shot.

Note where arrow hits and ask why.

Analysis - what were you thinking? 
