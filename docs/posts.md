---
title: Posts
ref: Modified 2022-02-14 
type: page
comment: AUTO GENERATED!
---
 
 
List of current posts:-
 
- [Unveiling the Universe with Emerging Cosmological Probes (2022-02-01)](/docs/cosmology-probes.html)
- [Memoirs - There are Programmers ... (2022-01-19)](/docs/memiors-programmers.html)
- [Epoch-alypse Now (2022-01-18)](/docs/2038.html)
- [Backgammon (2022-01-12)](/docs/backgammon.html)
- [Standard Sirens Update (2022-01-11)](/docs/cosmology-StandardSirens.html)
- [Why I Use Pandoc (2021-12-30)](/docs/about-this-site.html)
- [Local Hubble Parameter with 1 kms/s/Mpc Uncertainty (2021-12-16)](/docs/cosmology-Local-H0.html)
- [The Nobel Prize in Physics 2020 - Roger Penrose (2021-10-15)](/docs/physics-2020-Nobel.html)
- [Listening to The Big Bang (2021-08-01)](/docs/cosmology-ListenToTheBB.html)
- [First Solo (2021-07-05)](/docs/flying-FirstSolo.html)
- [Alan Turing - £50 Note (2021-03-25)](/docs/AlanTuring.html)
- [Memoirs - School Boy Error (2020-09-18)](/docs/memoirs-SchoolBoyError.html)
- [May the Force be with you (or not) ... (2018-05-20)](/docs/humour-TheForce.html)
- [Yer wern't 'it anything wi' that! (2018-05-02)](/docs/archery-Hit-Anything.html)
- [Clouds (2018-03-25)](/docs/clouds.html)
- [Stephen Hawking (2018-03-14)](/docs/physics-Stephen-Hawking.html)
- [Gravity (2017-09-21)](/docs/cosmology-Gravity.html)
- [Fokker (2017-09-07)](/docs/humour-fokker.html)
- [Can Dark Energy be Ditched? (2017-07-14)](/docs/cosmology-DarkEnergy.html)
- [The Archery 252 System (2017-07-05)](/docs/archery-252.html)
- [The Shot Cycle (2017-07-01)](/docs/archery-TheShotCycle.html)
- [Memoirs - Wang Car (2017-06-03)](/docs/memoirs-Wang-Car.html)
- [Memoirs - I Use 4 Screens (2017-05-17)](/docs/memoirs-I-Use-4-Screens.html)
 
 
23 entries
 
 
