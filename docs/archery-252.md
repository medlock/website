---
title: The Archery 252 System
type: post
ref: Published 2017-07-05
tags: Archery, 252, longbow
---

252 is a system to determine archers competence as the aim is to score 252 or more at a set distance with a fixed set of arrows 36 (6 arrows per end, 6 ends) using the imperial scoring system, on a 122cm face (a "ten" is the size of a CD).

Maximum score is thus 324. 

To get the 252 the archer needs to have an average score of 7 per arrow. 

Sounds easy, doesn't it? 

For a professional archer, yes, but for one that shoots once or twice a week it can be problematic!

*It's worth noting at this point that, at 70m (ie 70 metres), moving by just half a degree will result in missing the target face - completely!*

In the UK a 252 is used to enable the archer to progress to the next distance as the archer is required to do this a few times (normally 3) to show he/she can reliably shoot at the target at the distance (ie they are safe to operate a bow at said distance). 

**The Rules**

1) First shoot of the day (not allowed to practice then do a 252!) 
2) 6 sighter's allowed (used to assess the weather (ie wind), and new distance)
3) Imperial scoring (9,7,5,3,1,miss) 
4) 6 arrows per end  
5) 6 Ends
6) Required score >= 252
7) Officially, should have the score counter-signed by another archer for it to be valid! 

For the **longbow** the required score is **189** (in recognition that it is not as accurate as the recurve )! However, I still like to use 252 as the threshold when shooting the longbow.

*(Presumably, compound archers need a higher score on the same premise - it is much more accurate than recurve. But I don't know what that score is (not being a compound archer). It could be the same score on a smaller target face, I don't know.*