---
title: Cosmology Resources
type: page
ref: Published 2021-12-16
---

I'm not a professional cosmologist, but I have a strong physics and maths background.

If you are interested in cosmology and would like to learn more, perhaps read and understand scientific papers in the field, then take a look.

Below are the resources I have personally used to get "up-to-speed", so to speak.

**Books**

This download is dated 2006 but it is still good enough to get going

- [Introduction to Cosmology, Barbara Ryden (2006) (pdf)](http://atlas.physics.arizona.edu/~kjohns/downloads/lsst/Ryden_IntroCosmo.pdf)

A more recent edition (to purchase)

- [Introduction to Cosmology, Barbara Ryden (most recent) (link)](https://www.barnesandnoble.com/w/introduction-to-cosmology-barbara-ryden/1126351099)

**Papers**

- [ArXiv Pre-print Server](https://arxiv.org/list/astro-ph.CO/recent)


**Websites**

- [Cosmology Links](https://jila.colorado.edu/~ajsh/bh/cosmology.html)
- [LibreText](https://phys.libretexts.org/Courses/University_of_California_Davis/UCD%3A_Physics_156_-_A_Cosmology_Workbook/A_Cosmology_Workbook)
- [Ned Wright's Cosmology Tutorial](https://www.astro.ucla.edu/~wright/cosmo_01.htm)
- [Physics Forums](https://www.physicsforums.com/forums/cosmology.69/)
- [His Dark CMBlog](https://www.sunnyvagnozzi.com/blog) (Maybe a bit heavy if just starting out)


**YouTube**

- [Leonard Susskind Cosmology Course](https://inv.cthd.icu/watch?v=P-medYaqVak)
- [MIT (Alun Guth) Cosmology Course](https://inv.cthd.icu/playlist?list=PL4BUyFYOFtUZIUtcSDo6vm4HPxS9aZmHC) 
- [Cosmology Talks](https://www.youtube.com/channel/UCstdttIo3HM6h3hDk_v2hug) (Maybe a tad difficult for a beginner)
 