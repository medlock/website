---
title: Contact
type: page
ref: Published 2021-12-30
---


If you would like to contact me then please do.

Current methods:-

* E-mail: medlock {at} envs {dot} net
* Matrix: medlock:envs.net


Please be civil! 

I won't respond to blatant hostile negativity that the Internet seems to embolden people these days. There is really no excuse.

Remember the golden rule - **Treat people how you would like to be treated yourself**, and we'll get along famously.

**Constructive** criticism is always welcome!

Thanks, I look forward to hearing from you.
