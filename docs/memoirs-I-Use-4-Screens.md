---
title: Memoirs - I Use 4 Screens
type: post
ref: Published 2017-05-17
tags: memoirs
---

Short and sweet this one and long before the time of Windows, on real computers!

Sometimes we would use consultants to do work. One consultant would do his work in 4 logical sessions, this would allow 4 separate jobs on one connection (on one terminal) running concurrently. These could be separate editors for example - debugging, compiling, editing.

<img src="/resources/wang1.jpg" id="fig_pic_left"/>

Left is a typical terminal. These had limited power and were normally connected via co-ax directly to the computer or via WangNet (a pre-Ethernet type connection).

Despite their limited power, I know someone who wrote the [Pacman](https://pacman.cc/) game in assembler that ran on a terminal such as this. 

I'm feeling all nostalgic just looking at this. Those were the days! 

**When things just worked and hardly ever had any downtime!!**


Anyway, back to the story ...


Running 4 logical connections was a common technique, he wasn't alone - I did the same! 

In fact, it was expected so that time could be utilised effectively. After all, compilation/linking of a large program could take some time!

Well, this particular consultant always tried to maintain that because he did this, he should get paid 4 times as much. It didn't go down well, as one might expect.


He was actually very good at his job, but 4 times as much? - No way!


**If you were that consultant - you should still hold your head in shame!**

Get in touch, it would be great to reminisce about the great times we had.





 
