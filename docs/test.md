---
doctype: page
type: page
comment: 
title: test
ref: Published 2021-01-01, Modified 2021-12-30
---

A test page!

A First Level Header
====================

A Second Level Header
---------------------


# Header 1

## Header 2

### Header 3

#### Header 4



- [Local Hubble Parameter with 1 kms/s/Mpc Uncertainty (2021-12-16)](/docs//cosmology-Local-H0.html)
- [Listening to The Big Bang (2021-08-21)](/docs//cosmology-ListenToTheBB.html)


<img src="/resources/H0.png" id="fig_pic_left"/>

<br>
