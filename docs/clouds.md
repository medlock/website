---
title: Clouds
type: post
ref: Published 2018-03-25
tags: weather, flying, clouds
---


<h1>Six clouds you should know about – and what they can reveal about the weather</h1>

  <figure>
    <img src="https://images.theconversation.com/files/211216/original/file-20180320-31608-q93bgp.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=754&fit=clip" alt="File 20180320 31608 q93bgp.jpg?ixlib=rb 1.1" />
      <figcaption>
        Cumulonimbus: heavy rain and thunder on the horizon.
        <span class="attribution"><a class="source" href="https://www.shutterstock.com/download/success?src=q2vW9IZeYgVnBwhXpki_uQ-1-36">Shutterstock</a></span>
      </figcaption>
  </figure>

<span><a href="https://theconversation.com/profiles/hannah-christensen-192127">Hannah Christensen</a>, <em><a href="http://theconversation.com/institutions/university-of-oxford-1260">University of Oxford</a></em></span>

<p>Modern weather forecasts rely on <a href="https://www.theguardian.com/science/alexs-adventures-in-numberland/2015/jan/08/banking-forecasts-maths-weather-prediction-stochastic-processes">complex computer simulators</a>. These simulators use all the physics equations that describe the atmosphere, including the movement of air, the sun’s warmth, and the formation of clouds and rain. </p>

<p>Incremental improvements in <a href="https://www.nature.com/articles/nature14956">forecasts over time</a> mean that modern five-day weather forecasts are as skillful as three-day forecasts were <a href="https://www.ecmwf.int/en/newsletter/153/meteorology/25-years-ensemble-forecasting-ecmwf">20 years ago</a>.</p>

<p>But you don’t need a supercomputer to predict how the weather above your head is likely to change over the next few hours – this has been known across cultures <a href="https://earthobservatory.nasa.gov/Features/WxForecasting/wx2.php">for millennia</a>. By keeping an eye on the skies above you, and knowing a little about how clouds form, you can predict whether rain is on the way. </p>

<p>And moreover, a little understanding of the physics behind cloud formation highlights the complexity of the atmosphere, and sheds some light on why predicting the weather beyond a few days is such a challenging problem.</p>

<p>So here are six clouds to keep an eye out for, and how they can help you understand the weather.</p>

<h2>1) Cumulus</h2>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/211218/original/file-20180320-31617-mdry9x.jpeg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Cumulus: little white fluffy clouds.</span>
              <span class="attribution"><a class="source" href="https://www.pexels.com/photo/black-electric-posts-under-white-clouds-944532/">Brett Sayles/Pexels</a>, <a class="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY</a></span>
            </figcaption>
          </figure>

<p>Clouds form when air cools to the dew point, the temperature at which the air can no longer hold all its water vapour. At this temperature, water vapour condenses to form droplets of liquid water, which we observe as a cloud. For this process to happen, we require air to be forced to rise in the atmosphere, or for moist air to come into contact with a cold surface.</p>

<p>On a sunny day, the sun’s radiation heats the land, which in turn heats the air just above it. This warmed air rises by convection and forms <a href="https://cloudatlas.wmo.int/cumulus-cu.html">Cumulus</a>. These “fair weather” clouds look like cotton wool. If you look at a sky filled with cumulus, you may notice they have flat bases, which all lie at the same level. At this height, air from ground level has cooled to the dew point. Cumulus clouds do not generally rain – you’re in for fine weather.</p>

<h2>2) Cumulonimbus</h2>

<p>While small Cumulus do not rain, if you notice Cumulus getting larger and extending higher into the atmosphere, it’s a sign that intense rain is on the way. This is common in the summer, with morning Cumulus developing into deep <a href="https://cloudatlas.wmo.int/clouds-genera-cumulonimbus.html">Cumulonimbus</a> (thunderstorm) clouds in the afternoon. </p>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/211224/original/file-20180320-80621-hqypaj.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">A Cumulonimbus with its characteristic anvil shape.</span>
              <span class="attribution"><a class="source" href="https://www.shutterstock.com/download/success?src=q2vW9IZeYgVnBwhXpki_uQ-1-0">Shutterstock</a></span>
            </figcaption>
          </figure>

<p>Near the ground, Cumulonimbus are well defined, but higher up they start to look wispy at the edges. This transition indicates that the cloud is no longer made of water droplets, but ice crystals. When gusts of wind blow water droplets outside the cloud, they rapidly evaporate in the drier environment, giving water clouds a very sharp edge. On the other hand, ice crystals carried outside the cloud do not quickly evaporate, giving a wispy appearance.</p>

<p>Cumulonimbus are often flat-topped. Within the Cumulonimbus, warm air rises by convection. In doing so, it gradually cools until it is the same temperature as the surrounding atmosphere. At this level, the air is no longer buoyant so cannot rise further. Instead it spreads out, forming a characteristic anvil shape.</p>

<h2>3) Cirrus</h2>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/211225/original/file-20180320-80634-1l34xxc.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Cirrus clouds can mark the approach of a warm front – and rain.</span>
              <span class="attribution"><a class="source" href="https://www.shutterstock.com/download/success?src=5iuKxSxH11E16yXzxBJxiw-1-6">Shutterstock</a></span>
            </figcaption>
          </figure>

<p><a href="https://cloudatlas.wmo.int/cirrus-ci.html">Cirrus</a> form very high in the atmosphere. They are wispy, being composed entirely of ice crystals falling through the atmosphere. If Cirrus are carried horizontally by winds moving at different speeds, they take a characteristic hooked shape. Only at very high altitudes or latitudes do Cirrus produce rain at ground level.  </p>

<p>But if you notice that Cirrus begins to cover more of the sky, and gets lower and thicker, this is a good indication that a warm front is approaching. In a warm front, a warm and a cold air mass meet. The lighter warm air is forced to rise over the cold air mass, leading to cloud formation. The lowering clouds indicate that the front is drawing near, giving a period of rain in the next 12 hours.</p>

<h2>4) Stratus</h2>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/211220/original/file-20180320-31608-mzi52w.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Stratus: gloomy.</span>
              <span class="attribution"><span class="source">Hannah Christensen</span>, <span class="license">Author provided</span></span>
            </figcaption>
          </figure>

<p><a href="https://cloudatlas.wmo.int/stratus-st.html">Stratus</a> is a low continuous cloud sheet covering the sky. Stratus forms by gently rising air, or by a mild wind bringing moist air over a cold land or sea surface. Stratus cloud is thin, so while conditions may feel gloomy, rain is unlikely, and at most will be a light drizzle. Stratus is identical to fog, so if you’ve ever been walking in the mountains on a foggy day, you’ve been walking in the clouds.</p>

<h2>5) Lenticular</h2>

<p>Our final two cloud types will not help you predict the coming weather, but they do give a glimpse of the extraordinarily complicated motions of the atmosphere.  Smooth, lens-shaped <a href="https://cloudatlas.wmo.int/cm-4.html">Lenticular</a>
clouds form as air is blown up and over a mountain range. </p>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/211221/original/file-20180320-31633-1w5320k.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Lenticular clouds form over mountains.</span>
              <span class="attribution"><a class="source" href="https://www.shutterstock.com/download/success?src=TnAinooAC26sLJA-hJ0pzg-1-3">Shutterstock</a></span>
            </figcaption>
          </figure>

<p>Once past the mountain, the air sinks back to its previous level. As it sinks, it warms and the cloud evaporates. But it can overshoot, in which case the air mass bobs back up allowing another Lenticular cloud to form. This can lead to a string of clouds, extending some way beyond the mountain range. The interaction of wind with mountains and other surface features is one of the many details that have to be represented in computer simulators to get accurate predictions of the weather.</p>

<h2>6) Kelvin-Helmholtz</h2>

<p>And lastly, my personal favourite. The <a href="https://cloudatlas.wmo.int/clouds-supplementary-features-fluctus.html">Kelvin-Helmholtz</a>
cloud resembles a breaking ocean wave. When air masses at different heights move horizontally with different speeds, the situation becomes unstable. The boundary between the air masses begins to ripple, eventually forming larger waves. </p>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/211250/original/file-20180320-80634-70c377.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Kelvin-Helmholtz clouds resemble breaking waves in the ocean.</span>
              <span class="attribution"><a class="source" href="https://opensky.ucar.edu/islandora/object/imagegallery%3A163">NCAR UCAR OpenSky Repository</a>, <a class="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA</a></span>
            </figcaption>
          </figure>

<p><img src="https://counter.theconversation.com/content/93402/count.gif?distributor=republish-lightbox-basic" alt="The Conversation" width="1" height="1" />Kelvin-Helmholtz clouds are rare – the only time I spotted one was over Jutland, western Denmark – because we can only see this process taking place in the atmosphere if the lower air mass contains a cloud. The cloud can then trace out the breaking waves, revealing the intricacy of the otherwise invisible motions above our heads.</p>

<p><span><a href="https://theconversation.com/profiles/hannah-christensen-192127">Hannah Christensen</a>, Visiting Researcher, Atmospheric Oceanic and Planetary Physics, <em><a href="http://theconversation.com/institutions/university-of-oxford-1260">University of Oxford</a></em></span></p>

<p>This article was originally published on <a href="http://theconversation.com">The Conversation</a>. Read the <a href="https://theconversation.com/six-clouds-you-should-know-about-and-what-they-can-reveal-about-the-weather-93402">original article</a>.</p>


