---
title: Operational Research - Techniques
type: page
ref: Published 2021-05-03, Modified 2021-07-23
tags: OR, techniques
---

Here is a list of techniques used:-


**Linear Programming**

This is a constrained optimisation technique, that optimises some criterion within some constraints. In
linear programming the objective function (profit, loss or return on investment) and constraints are
linear. There are different methods available to solve linear programming.

**Game Theory**

This is used for making decisions under conflicting situations where there are one or more
players/opponents. In this the motive of the players are dichotomised. The success of one player tends to
be at the cost of other players and hence they are in conflict.

**Decision Theory**

Decision theory is concerned with making decisions under conditions of complete certainty about the
future outcomes and under conditions such that we can make some probability about what will happen
in the future.

**Queuing Theory**

This is used in situations where the queue is formed (for example customers waiting for service, aircraft
waiting for landing, jobs waiting for processing in the computer system, etc). The objective here is
minimizing the cost of waiting without increasing the cost of servicing.

**Inventory Models**

Inventory model make decisions that minimize total inventory cost. This model successfully reduces
the total cost of purchasing, carrying, and out of stock inventory.

**Simulation**

Simulation is a procedure that studies a problem by creating a model of the process involved in the
problem and then through a series of organized trials and error solutions attempt to determine the best
solution. Some times this is a difficult/time-consuming procedure. 

Simulation is used when actual experimentation is not feasible or solution of model is not possible.

**Non-linear Programming**

This is used when the objective function and the constraints are not linear in nature. Linear relationships
may be applied to approximate non-linear constraints but limited to some range, because approximation
becomes poorer as the range is extended. Thus, the non-linear programming is used to determine the
approximation in which a solution lies and then the solution is obtained using linear methods.

**Dynamic Programming**

Dynamic programming is a method of analysing multi-stage decision processes. In this each elementary
decision depends on those preceding decisions and as well as external factors.

**Integer Programming**

If one or more variables of the problem take integral values only then dynamic programming method is
used. For example number or motor in an organization, number of passenger in an aircraft, number of
generators in a power generating plant, etc.

**Markov Process**

Markov process predicts changes over time information about the behaviour of a system is
known. This is used in decision-making in situations where the various states are defined. The
probability from one state to another state is known and depends on the current state and is independent
of how we have arrived at that particular state.

**Network Scheduling**

This technique is used extensively to plan, schedule, and monitor large projects (for example computer
system installation, R & D design, construction, maintenance, etc.). The aim of this technique is tp
minimise trouble spots (such as delays, interruption, production bottlenecks, etc.) by identifying the
critical factors. The different activities and their relationships of the entire project are represented
diagrammatically with the help of networks and arrows, which is used for identifying critical activities
and path. There are two main types of technique in network scheduling, they are:
Program Evaluation and Review Technique (PERT) – is used when activities time is not known
accurately/ only probabilistic estimate of time is available.

Critical Path Method (CPM) – is used when activities time is know accurately.

**Information Theory**

This analytical process is transferred from the electrical communication field to OR field. The
objective of this theory is to evaluate the effectiveness of flow of information with a given system. This
is used mainly in communication networks but also has indirect influence in simulating the examination
of business organizational structure with a view of enhancing flow of information.