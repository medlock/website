---
title: Alan Turing - £50 Note
type: post
ref: Published 2021-03-25
tags: Turing, genius, £50
---

Today marks the unveiling of the new £50 note from the Bank of England featuring [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing).

It will be officially released on 23rd June to mark his birthday.

Alan Turing was treated very badly by the Government of his time. He was a homosexual at a time it was illegal in Britain. Yet, due to bigotry and intolerance, the world lost a true **genius**.

We will never know what he could have gone on to achieve, his short life, cut down by the idiocy of stupid ill-thought-out laws.

**Let that be a lesson to us all!** 

<img src="/resources/50_Turing.jpg" id="fig_pic_left"/>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
