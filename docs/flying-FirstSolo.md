---
title: First Solo
type: post
ref: Published 2021-07-05
tags: flying
---

**I can't believe how quickly this has gone.**

On this day 35 years ago (1986) I completed my first solo in a Cessna 150 (G-BAZS) - time flies.

I remember like it happened yesterday. **Exercise 14.** There is only one "Ex 14" in a flying logbook!

My instructor for that day (the Chief Flying Instructor (CFI) no less), rated my landing as "above average". 
RAF speak for very good!

He would later go on to teach me aerobatics, which was great fun! 

Being taught by an Ex World War II pilot was fantastic fun and something I will always, always cherish. 

**Thank you Jacko!**

<img src="/resources/FirstSolo.jpg" id="fig_pic_left"/>

<br>
<br>
Here is my rather humorous certificate.

I love the passage ...

 *... who, nothing daunted, did clatter off into the blue, circumnavigate the airfield and land gaily, proving that the age of miracles is not yet dead.*

Brilliant!


If I remember rightly, it was quite a murky day with altostratus clouds @ 6,000 ft, so hardly blue, but I love the sentiment.

It also happened all far too quickly as well. I wanted to do again, but a prior engagement put pay to that.

I also received some wings, which I proudly wear to this day on my flying jacket.

From that day on it would be a few lessons doing solo circuit work to build up the hours before moving on to the navigation part of the course.


<br>
<br>
<br>
<br>