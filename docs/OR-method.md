---
title: Operational Research - Method
type: page
ref: Published 2021-05-03
tags: OR, methodology
---

The stages of OR, often referred to as phases, are:-

1. Observe the problem environment
2. Analyse and define the problem
3. Develop a model
4. Select appropriate data input
5. Provide a solution and test its reasonableness
6. Implement the solution

