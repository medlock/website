---
title: Gravity
type: post
ref: Published 2017-09-21
tags: physics, cosmology, gravity, Verlinde
---

Check your dairies for 4th October 2017 @ 19:00 ET (5th October 2017 @ 00:00 proper time, GMT, UTC, Zulu), for a [webcast](https://insidetheperimeter.ca/a-new-view-on-gravity-and-the-dark-side-of-the-cosmos-erik-verlinde-public-lecture/?utm_term=VERLINDE&utm_campaign=PIPLS&utm_content=60569008&utm_medium=social&utm_source=twitter) of a new theory about gravity at the [Perimeter Institute](https://insidetheperimeter.ca/) in Canada.

I have been following this for some time and [Dr Erik Verlinde](https://twitter.com/erikverlinde?lang=en) of Amsterdam University has come up with a new way to describe [gravity](https://arxiv.org/abs/1611.02269), and in doing so eliminates the need for dark matter. 

It completely explains why gravity exists and just doesn't assume it exists like Newton and Einstein.

Why is gravity so weak compared to the other forces? Is it a fundamental force of nature or did it emerge after the Big Bang? What is wrong with our current understanding of gravity.

Lovely stuff, can't wait!!

