---
title: Why I Use Pandoc
type: post
ref: Published 2021-12-30
---

Being a lazy git, and knowing sweet-f*-all about web design/programming, I have been a long time user of [Jekyll](https://jekyllrb.com/) and more recently [Hugo](https://gohugo.io/). 

I never have liked HTML / CSS, so I have never wanted to get my "hands dirty" using it. I have always looked upon HTML as being far too complicated for what it needs to be. (oh, the irony from one coming from an assembler, C, bash background).

However, recently I have become increasing disillusioned about these site generators. Don't get me wrong, they are great and I appreciate what they do. The problem is they never do exactly what is wanted. One always has to modify something. Then there is always a compatibility issue with the chosen theme. Having to use raw HTML to do something (trivial) in Markdown, or it's the way tables are processed or vice versa. Or something else. 

**It drives me nuts!**

The thing is, I'm always having to alter something in a "framework" I loathe. Not to mention one has to start looking at what seems like pages and pages of CSS or HTML and change/hack to get it to work as required. 

I hate debugging other people's code. Ha! I even hate debugging my own code - particularly stuff I wrote a long time ago! Some of my stuff dates back over 40 years, and I sometimes think "F*! Did I really do it that way - what was I thinking?".

So a few days ago I had an epiphany. Why? I thought.

I just want a simple easy to use site that uses Markdown, something that fits in with the ethos of a tilde system (yes I was also feeling "guilty" in using Hugo on a system that promotes the simplicity of the early web). Something that is easy to maintain and does EXACTLY what I want it to do.

I thought of [Pandoc](https://pandoc.org), a tool I have used before on other projects. I could use it to build a site. How hard could it be? Why didn't I think of this before!!??

After a quick search, I found a number of people have addressed this exact scenario, and I was immediately drawn to a system called [TSPW](https://github.com/eakbas/TSPW). It has a really simple structure and the HTML and CSS is easy to follow that even I can understand, and yes begrudgingly, say that I'm actually starting to enjoy this. 

There, I have said it, I'm actually enjoying coding in HTML and CSS. Who would have thought it.

Never say "can't teach an old dog new tricks!". 

=> [Site to-do list](todo.html)
