---
title: Local Hubble Parameter with 1 kms/s/Mpc Uncertainty
type: post
ref: Published 2021-12-16
tags: physics, cosmology, Hubble
---

The 8th December saw the release of most recent study of the so-called "Hubble Tension" using Hubble Space Telescope and the SH0ES Team.

Here is the abstract -

<p class="Abst">
We report observations from HST of Cepheids in the hosts of 42 SNe Ia used to calibrate the Hubble constant (H0). These include all suitable SNe Ia in the last 40 years at z<0.01, measured with >1000 orbits, more than doubling the sample whose size limits the precision of H0. The Cepheids are calibrated geometrically from Gaia EDR3 parallaxes, masers in N4258 (here tripling that Cepheid sample), and DEBs in the LMC. The Cepheids were measured with the same WFC3 instrument and filters (F555W, F814W, F160W) to negate zeropoint errors.
<p>
<p class="Abst">
We present multiple verifications of Cepheid photometry and tests of background determinations that show measurements are accurate in the presence of crowding. The SNe calibrate the mag-z relation from the new Pantheon+ compilation, accounting here for covariance between all SN data, with host properties and SN surveys matched to negate differences. We decrease the uncertainty in H0 to 1 km/s/Mpc with systematics. We present a comprehensive set of ~70 analysis variants to explore the sensitivity of H0 to selections of anchors, SN surveys, z range, variations in the analysis of dust, metallicity, form of the P-L relation, SN color, flows, sample bifurcations, and simultaneous measurement of H(z).
<p>
<p class="Abst">
Our baseline result from the Cepheid-SN sample is H0=73.04+-1.04 km/s/Mpc, which includes systematics and lies near the median of all analysis variants. We demonstrate consistency with measures from HST of the TRGB between SN hosts and NGC 4258 with Cepheids and together these yield 72.53+-0.99. Including high-z SN Ia we find H0=73.30+-1.04 with q0=-0.51+-0.024. We find a 5-sigma difference with H0 predicted by Planck+LCDM, with no indication this arises from measurement errors or analysis variations considered to date. The source of this now long-standing discrepancy between direct and cosmological routes to determining the Hubble constant remains unknown.
</p>

Paper can be found here [arXiv:2112.04510](https://arxiv.org/abs/2112.04510)

The upshot is that they claim the result is a 5-sigma difference between the Cosmic Microwave Background (CMB) data (aka early-time) and local values (aka late-time) observations. This is significant and it's looking like new physics **may be** at play here to account for the difference.

The Hubble parameter can not be inferred directly from the CMB, it has to be calculated. Moreover, the value is **extrapolated** from early time data to present day.

The process, in principle is simple, however extremely technical in nature. [This popular science article](https://www.forbes.com/sites/startswithabang/2021/01/15/ask-ethan-how-does-the-cmb-reveal-the-hubble-constant/) explains the process.


It is worth pointing out that the difference between CMB and local-time data is 9%. Think about it, 9% over the time since when the CMB was formed to present day.

<img src="/resources/H0.png" id="fig_pic_left"/>

<br>

However, (see left) it is also very interesting, perhaps telling, that values of H0 from CMB and gravity waves agree with each other and the optical methods agree with each other. There are clear groups.

Blue denotes early time results, red late time.

So is there new physics? 

I don't know, I don't think so. 

To me the fact that these two groups differ by a mere 9% shows that perhaps something is being overlooked.

A lot of work has gone into this paper, it truly is amazing, thorough work. Please have a read.

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

Here is a link from the Cosmology Talks channel where Dillon Brout, Adam Riess and Dan Scolnic talk about the latest SH0ES measurement of the Hubble parameter, making use of the new Pantheon+ supernovae data set.

(Opens a new window)

[![IMAGE ALT TEXT](http://img.youtube.com/vi/WFKzEtScvw4/0.jpg)](http://www.youtube.com/watch?v=WFKzEtScvw4E "Cosmology Talks")
