---
title: About
type: page
ref: Published 2021-12-30
---

I'm a retired Systems Engineer. 

Despite having a lot of time on my hands, I have far too many interests for my own good, and **definitely not enough time** to do what I want to do!!

I have an honours degree (BSc) in [Operational Research](/docs/OR.html) & Computing and have spent most of my working life in IT.

My main interests:-

* Archery
* [Backgammon](/docs/backgammon.html) 
* [Cosmology](/docs/cosmology.html)
* Flying
* IT (still like to "keep my hand in")   

I use the following systems:-

* Laptop - [Artix Linux](https://artixlinux.org/) + [i3](https://i3wm.org/)
* Home server - [FreeBSD](https://www.freebsd.org/)
* Playground/lab  - [Arduinos](https://www.arduino.cc/), [Raspberry Pis](https://www.raspberrypi.org/)
 

I would like to close in **thanking my lovely wife**. Who, despite the years, still "puts-up" with me and, without whom, I would be **totally lost**!

