---
comment: AUTO GENERATED! - Edit template version of this file.
pagetitle: envs.net/~medlock
type: page
ref: 
---
Welcome to my area of the "Interweb".

<br>

Most recent posts:-

  - [Unveiling the Universe with Emerging Cosmological Probes (2022-02-01)](/docs/cosmology-probes.html)
  - [Memoirs - There are Programmers ... (2022-01-19)](/docs/memiors-programmers.html)
  - [Epoch-alypse Now (2022-01-18)](/docs/2038.html)
  - [Backgammon (2022-01-12)](/docs/backgammon.html)
  - [Standard Sirens Update (2022-01-11)](/docs/cosmology-StandardSirens.html)
  - [Why I Use Pandoc (2021-12-30)](/docs/about-this-site.html)
 
<br> 
Last Updated 16 Feb 2022 @ 08:20:00 
