---
title: Memoirs - Wang Car
type: post
ref: Published 2017-06-03
tags: memoirs, Wang, VS, humour
---

In the early days of my IT career I worked in the financial sector and the systems we used were [Wang](https://en.wikipedia.org/wiki/Wang_Laboratories) & [HP UX](https://en.wikipedia.org/wiki/HP-UX). 

I have very fond memories using Wang equipment (others hated it), but I quite liked Wang - particularly the VS ranges. In many respects Wang offered technologies far ahead of their time, and the equipment was well-made and robust. Only one other manufacturer even comes close, and that is HP.

I can always remember seeing inside one of the VS's, a [VS 100](https://www.ricomputermuseum.org/collections-gallery/equipment/wang-vs100), for the first time. There were no sharp edges, everything finished to a very high standard. It was perfectly safe for an engineer to work on without injury. Unlike the abominations we have today.

More stories I may explore later, back to this one.


It was standard to send employees on courses and I went on a lot of courses to Wang in London. Often the courses were over several days so had to be put up in a hotel. Other computer companies course delegate's would stay at the same hotel, like DEC, IBM, ICL, HP, etc.

Transport was provided to and from the hotel for the respective courses, so after breakfast, we would go to the foyer of the hotel and wait for collection. The concierge would announce the arrival of transport.

"HP Car!", all the HP delegates would go. 
Then, "IBM Car!" - IBM delegates get up and proceed outside. 

It would carry on like this for all the manufacturers ...



"Wang Car!", the concierge shouted. 



All the Wang delegate's would look sheepishly at each other and snigger - before getting up and proceeding to the car.

Ah yes, you have guessed it - sounds like **wanker!**


We don't think the concierge actually knew what he was saying - well, he never let on at any rate!


If you attended any Wang courses, or perhaps remember something like this, then please get in touch. 
