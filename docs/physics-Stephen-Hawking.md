---
title: Stephen Hawking
type: post
ref: Published 2018-03-14
tags: physics, Hawking
---

<h1>Stephen Hawking: Martin Rees looks back on colleague's spectacular success against all odds</h1>

  <figure>
    <img src="https://images.theconversation.com/files/210291/original/file-20180314-113462-12zuv3e.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=754&fit=clip" alt="File 20180314 113462 12zuv3e.jpg?ixlib=rb 1.1" />
      <figcaption>
        
        <span class="attribution"><span class="source">Lwp Kommunikáció/Flickr</span>, <a class="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a></span>
      </figcaption>
  </figure>

<span><a href="https://theconversation.com/profiles/martin-rees-100656">Martin Rees</a>, <em><a href="http://theconversation.com/institutions/university-of-cambridge-1283">University of Cambridge</a></em></span>

<p>Soon after I enrolled as a graduate student at Cambridge University in 1964, I encountered a fellow student, two years ahead of me in his studies, who was unsteady on his feet and spoke with great difficulty. This was Stephen Hawking. He had recently been <a href="http://www.abc.net.au/news/health/2018-03-14/stephen-hawking-als-how-he-outlived-his-prognosis-for-so-long/9548110">diagnosed with a degenerative disease</a>, and it was thought that he might not survive long enough even to finish his PhD. But he lived to the age of 76, <a href="https://www.theguardian.com/science/2018/mar/14/stephen-hawking-professor-dies-aged-76">passing away on March 14, 2018</a>.  </p>

<p>It really was astonishing. Astronomers are used to large numbers. But few numbers could be as large as the odds I’d have given against witnessing this lifetime of achievement back then. Even mere survival would have been a medical marvel, but of course he didn’t just survive. He became one of the most famous scientists in the world – acclaimed as a world-leading researcher in mathematical physics, for his best-selling books and for his astonishing triumph over adversity. </p>

<p>Perhaps surprisingly, Hawking was rather laid back as an undergraduate student at Oxford University. Yet his brilliance earned him a first class degree in physics, and he went on to pursue a research career at the University of Cambridge. Within a few years of the onset of his disease, he was wheelchair-bound, and his speech was an indistinct croak that could only be interpreted by those who knew him. In other respects, fortune had favoured him. He married a family friend, Jane Wilde, who provided a supportive home life for him and their three children.</p>

<h2>Early work</h2>

<p>The 1960s were an exciting period in astronomy and cosmology. This was the decade when evidence <a href="https://www.space.com/25126-big-bang-theory.html">began to emerge</a> for black holes and the Big Bang. In Cambridge, Hawking focused on the new mathematical concepts being developed by the mathematical physicist Roger Penrose, <a href="http://www.ucl.ac.uk/livergroup/trustee.html">then at University College London</a>, which were initiating a renaissance in the study of Einstein’s theory of general relativity. </p>

<p>Using these techniques, Hawking worked out that the universe must have emerged from a “singularity” – a point in which all laws of physics break down. He also realised that the area of a black hole’s event horizon – a point from which nothing can escape – could never decrease. In the subsequent decades, the observational support for these ideas has strengthened – most spectacularly with the <a href="https://theconversation.com/explainer-what-are-gravitational-waves-53239">2016 announcement</a> of the detection of gravitational waves from colliding black holes.</p>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/210267/original/file-20180314-113479-1pcd4vl.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Hawking at the University of Cambridge.</span>
              <span class="attribution"><span class="source">Lwp Kommunikáció/Flickr</span>, <a class="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a></span>
            </figcaption>
          </figure>

<p>Hawking was elected to the Royal Society, Britain’s main scientific academy, at the exceptionally early age of 32. He was by then so frail that most of us suspected that he could scale no further heights. But, for Hawking, this was still just the beginning. </p>

<p>He worked in the same building as I did. I would often push his wheelchair into his office, and he would ask me to open an abstruse book on quantum theory – the science of atoms, not a subject that had hitherto much interested him. He would sit hunched motionless for hours – he couldn’t even to turn the pages without help. I remember wondering what was going through his mind, and if his powers were failing. But within a year, he came up with his best ever idea – encapsulated in an equation that he said he wanted on his memorial stone.</p>

<h2>Scientific stardom</h2>

<p>The great advances in science generally involve discovering a link between phenomena that seemed hitherto conceptually unconnected. Hawking’s “eureka moment” revealed a profound and unexpected link between gravity and quantum theory: he predicted that black holes would not be completely black, but would radiate energy in a characteristic way. </p>

<p>This radiation is only significant for black holes that are much less massive than stars – and none of these have been found. However, “Hawking radiation” had very deep implications for mathematical physics – indeed one of the main achievements of a theoretical framework for particle physics called <a href="https://theconversation.com/explainer-string-theory-2983">string theory</a> has been to corroborate his idea. </p>

<p>Indeed, the string theorist <a href="https://www.physics.harvard.edu/people/facpages/strominger">Andrew Strominger</a> from Harvard University (with whom Hawking recently collaborated) said that this paper had caused “<a href="https://www.nature.com/articles/d41586-018-02839-9">more sleepless nights</a> among theoretical physicists than any paper in history”. The key issue is whether information that is seemingly lost when objects fall into a black hole is in principle recoverable from the radiation when it evaporates. If it is not, this violates a deeply believed principle of general physics. Hawking initially thought such information was lost, <a href="https://en.wikipedia.org/wiki/Thorne%E2%80%93Hawking%E2%80%93Preskill_bet">but later changed his mind</a>.</p>

<p>Hawking continued to seek new links between the very large (the cosmos) and the very small (atoms and quantum theory) and to gain deeper insights into the very beginning of our universe – addressing questions like <a href="https://www.theatlantic.com/science/archive/2016/08/the-multiverse-as-imagination-killer/497417/">“was our big bang the only one?</a>”. He had a remarkable ability to figure things out in his head. But he also worked with students and colleagues who would write formulas on a blackboard – he would stare at it, say whether he agreed and perhaps suggest what should come next. </p>

<p>He was specially influential in his contributions to “<a href="http://www.ctc.cam.ac.uk/outreach/origins/inflation_zero.php">cosmic inflation</a>” – a theory that many believe describes the ultra-early phases of our expanding universe. A key issue is to understand the primordial seeds which eventually develop into galaxies. Hawking proposed (as, independently, did the Russian theorist Viatcheslav Mukhanov) that these were “quantum fluctuations” (temporary changes in the amount of energy in a point in space) – somewhat analogous to those involved in “Hawking radiation” from black holes. </p>

<p>He also made further steps towards linking the two great theories of 20th century physics: the quantum theory of the microworld and Einstein’s theory of gravity and space-time. </p>

<h2>Declining health and cult status</h2>

<p>In 1987, Hawking contracted  pneumonia. He had to undergo a tracheotomy, which removed even the limited powers of speech he then possessed. It had been more than ten years since he could write, or even use a keyboard. Without speech, the only way he could communicate was by directing his eye towards one of the letters of the alphabet on a big board in front of him. </p>

<p>But he was saved by technology. He still had the use of one hand; and a computer, controlled by a single lever, allowed him to spell out sentences. These were then declaimed by a speech synthesiser, with the androidal American accent that thereafter became his trademark. </p>

<p>His lectures were, of course, pre-prepared, but conversation remained a struggle. Each word involved several presses of the lever, so even a sentence took several minutes to construct. He learnt to economise with words. His comments were aphoristic or oracular, but often infused with wit. In his later years, he became too weak to control this machine effectively, even via facial muscles or eye movements, and his communication – to his immense frustration – became even slower. </p>

<figure class="align-center ">
            <img alt="" src="https://images.theconversation.com/files/210264/original/file-20180314-113472-lfceh0.jpg?ixlib=rb-1.1.0&amp;q=45&amp;auto=format&amp;w=754&amp;fit=clip">
            <figcaption>
              <span class="caption">Hawking in zero gravity.</span>
              <span class="attribution"><span class="source">NASA</span></span>
            </figcaption>
          </figure>

<p>At the time of his tracheotomy operation, he had a rough draft of a book, which he’d hoped would describe his ideas to a wide readership and earn something for his two eldest children, who were then of college age. On his recovery from pneumonia, he resumed work with the help of an editor. When the US edition of <a href="https://www.theguardian.com/science/2018/mar/14/a-brief-history-of-stephen-hawkings-brief-history-of-time">A Brief History of Time</a> appeared, the printers made some errors (a picture was upside down), and the publishers tried to recall the stock. To their amazement, all copies had already been sold. This was the first inkling that the book was destined for runaway success, reaching millions of people worldwide.</p>

<p>And he quickly became somewhat of a cult figure, featuring on <a href="https://www.theguardian.com/science/2018/mar/14/from-the-simpsons-to-pink-floyd-stephen-hawking-in-popular-culture">popular TV shows</a> ranging from the Simpsons to The Big Bang Theory. This was probably because the concept of an imprisoned mind roaming the cosmos plainly grabbed people’s imagination. If he had achieved equal distinction in, say, genetics rather than cosmology, his triumph probably wouldn’t have achieved the same resonance with a worldwide public. </p>

<p>As shown in the feature film <a href="https://theconversation.com/the-theory-of-everything-is-inspiring-despite-a-hackneyed-treatment-of-hawkings-work-35348">The Theory of Everything</a>, which tells the human story behind his struggle, Hawking was far from being the archetype unworldy or nerdish scientist. His personality remained amazingly unwarped by his frustrations and handicaps. He had robust common sense, and was ready to express forceful political opinions. </p>

<p>However, a downside of his iconic status was that that his comments attracted exaggerated attention even on topics where he had no special expertise – for instance, philosophy, or the dangers from aliens or from intelligent machines. And he was sometimes involved in media events where his “script” was written by the promoters of causes about which he may have been ambivalent.</p>

<p><img src="https://counter.theconversation.com/content/93379/count.gif?distributor=republish-lightbox-basic" alt="The Conversation" width="1" height="1" />Ultimately, Hawking’s life was shaped by the tragedy that struck him when he was only 22. He himself said that everything that happened since then was a bonus. And what a triumph his life has been. His name will live in the annals of science and millions have had their cosmic horizons widened by his best-selling books. He has also inspired millions by a unique example of achievement against all the odds – a manifestation of amazing willpower and determination.</p>

<p><span><a href="https://theconversation.com/profiles/martin-rees-100656">Martin Rees</a>, Emeritus Professor of Cosmology and Astrophysics, <em><a href="http://theconversation.com/institutions/university-of-cambridge-1283">University of Cambridge</a></em></span></p>

<p>This article was originally published on <a href="http://theconversation.com">The Conversation</a>. Read the <a href="https://theconversation.com/stephen-hawking-martin-rees-looks-back-on-colleagues-spectacular-success-against-all-odds-93379">original article</a>.</p>
