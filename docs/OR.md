---
title: Operational Research
type: page
ref: Published 2021-05-03, Modified 2021-05-24
---

"What is Operational Research?", I hear you shout.

Well, I'm glad you asked.

Operational Research (OR) is the use of analytical methods to solve (complex) problems and aid in decision-making.

Today, it is used in many areas: business, the armed forces, sports. These are just a few. 

As a discipline, it was invented in Britain during World War II. However, historically, certain tools that OR practitioners use, have been used much earlier. 

Charles Babbage is said to have used it in his study of "[On the Economy of Machinery and Manufacture](https://monoskop.org/images/a/a1/Babbage_Charles_On_the_Economy_of_Machinery_and_Manufactures.pdf)" in 1832. Even Leonardo Da Vinci has been linked to OR, and other historical notables (eg Thomas Edison, Jon Van Neumann).

I don't intend to go into all this, as they used specific tools to solve problems, as used in applied maths. We have all used applied mathematics to solve a problem (even though you probably didn't realise it). 

A typical trivial problem might be to work out the price of items using simultaneous equations. 

This, however, is NOT Operational Research, it's applied maths! Yes, branches of maths are some of the tools used, but they do not constitute OR in its entirety.

OR has a [methodology](/docs/OR-method.html) and a "toolbox" of [techniques](/docs/OR-tech.html). Indeed, it uses a systematic approach to solving problems. It is used as a aid to help the decision process as the solution(s) can be rejected.

The one thing that is fair to say is that without the computer, many of the problems we look at today simply would not be possible.

As an undergraduate studying OR in the early 80's, I found the subject both fascinating and challenging. The course was one of a handful of undergraduate ones available in the UK. OR was (it might even still be now, I don't know) deemed a postgraduate subject, and with hindsight I understand why. It was hard work! There was **a lot of subject matter to study and master!** It also requires a certain, shall I say, "mature" mindset.

There are many OR **cases studies**, some absolutely fascinating. These are seen as an essential tool to learn the trade. Unfortunately, I can't publish the ones I did in industry after I got my degree as they are company confidential (and I don't have the docs anyway), but I do intend to go through studies that are in the public domain.

The first case study I want to have a look at is the first one I was taught as an undergraduate.

+ World War II, Improving the RAF U-boat kill rate in the Atlantic.

<br>
In the mean time, whilst I find the time to do this, if you would like to learn more, please have a look -

- [OR-Notes](http://people.brunel.ac.uk/~mastjjb/jeb/or/contents.html)
- [The OR Society](https://www.theorsociety.com/)