---
title: Memoirs - There are Programmers ...
type: post
ref: Published 2022-01-19
tags: memoirs, programming
---

My very first job in IT was in the financial sector and I fell under the wing of an Analyst Programmer called Paul, who looked after me and guided me through this time. Despite our similar ages (he was a few years older), Paul was a very experienced IT professional - having chosen not to do a degree.

We immediately got on with each other, sharing many common grounds, and he would often tease me or make fun of something I'd done. 

Paul's favourite saying was "**There are programmers, then there are PROGRAMMERS**". 

Every time I think of this I always hear it in Paul's [Mancunian](https://en.wikipedia.org/wiki/Manchester_dialect) accent. He also had a very dry sense of humour and always said this with a knowing smile and a twinkle in his eye.

So what does it mean?

Let me rephrase it "There are programmers, then there are so-called PROGRAMMERS.

Any clearer?

Well it means that there are people who can program, and then there are people who can't (but think they can).

We normally use it to contrast people that are good at something with those that are bad.

We could just as easily say -

- There are drivers, then there are drivers.
- There are teachers, then there are teachers.
- There are managers, then there are managers.

etc etc.

I'm pretty sure that, in my early days, he was aiming this gibe at me. But overtime, it was apparent that was not the case - I had graduated to a real programmer!

You see, coming out of academia, I was into programming in the most efficient way possible. 
Everything was done this way.

But Paul was more pragmatic. 

He had to be, responsible for delivering projects on time, projects would often reuse existing code.

He would sometimes say that "one day, some poor sod might have to modify this. He won't understand it, and will probably have to start a fresh". 

Paul would go on to say " ...  changing a simple 10-minute job, into a day or two, or even worse - weeks ...".

**Thus, severely impacting project delivery.**

The moral of the story?

- Use a language appropriate for the task
- Comment it
- Only make the parts of it that need to be efficient - efficient
- Above all, comply with the company programming standards. If there aren't any (unusual these days), ask!

**So, why am I telling this story?**

Well, I think it's a lovely story, and brings back a lot of great memories.

But, more importantly, I see **badly written programs today**. 
Nobody seems to have learnt anything!

So, if you are programmer, just think on - do yourself a favour and remember the four points above.

<br>
<br>
~~~

Paul eventually left, tired of the daily commute. Which was a shame, he was very good at his job, and would have been great to have around in the intervening years after his departure.

We sort of lost touch too.


<br>
If you have stumbled across this Paul, I hope you and your family are well.

I often think of the times we had together. Good times.

**Thank you for your generous guidance during my early days!**
