---
title: Memoirs - Supplementary
type: page
ref: Published 2020-09-18, Modified 2022-01-12
---


I have had the pleasure of meeting some real characters, most of them nice, most were an absolute pleasure to work with too.

But, I have to say, I have also had the misfortune to come across some absolute twats too.

For some reason IT seems to attract some real "undesirables". I'm sure this happens in other fields too, but IT does have more than its fair-share of despots.

Many of these people should never have been in IT what-so-ever. From pseudo-hippies, to people that just knew the lingo and had nothing of substance to offer. Whiteboard hoggers - who just wanted to be in the limelight, and definitely weren't interested in what others had to say.

The ones who played "office politics" to promote nothing more than themselves at the expense of everyone else, and ultimately to the detriment of the company. The "CV Gold-diggers", who would champion a technology so that they could include it on their CV, then would bugger off - once they had gained enough experience.

**Honour-less people!**

Yes I have met them all, which on the bounds of probabilities, was inevitable - sadly.

I don't intend to use this space to dwell on the actions of those cretins. They tried to make my life a misery, and failed - miserably. Besides, they used enough of my time in the past and they don't get to use it now! 

Unless, of course, I can use the stories to mock them! 

Goes without saying. ;-)