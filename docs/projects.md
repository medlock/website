---
title: Projects
type: page
ref: Published 2020-03-30, Modified 2021-12-30
tags: projects
---

Things I'm working on:-

**Archery**

- Making a new English Longbow 


**IT**

- Garmin Analogue watch face for a Venu Sq
- Improving the generator for this site
- Using a RTL-SDR dongle to do radio astronomy
- Bring back to life a Wang VS5000 mini computer
