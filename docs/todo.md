---
title: To-do 
type: page
ref: Published 2021-12-30, Modified 2022-01-05
tags: projects, to-do
---

Totally fed-up with the likes of [Jekyll](https://jekyllrb.com/) and more recently [Hugo](https://gohugo.io/), I have decided to simplify things and use [Pandoc](https://pandoc.org) to generate this site, as a result I have lost functionality that comes with using the above static site generators.

Things I need to do to improve this site:-

- tags / categories
- bash script to generate output
- photo gallery
- RSS feed / sitemap

Privacy related improvements

~~Site to use local fonts (prevents tracking)~~  Done, Jan 2022


**This site respects your privacy!**

- There are no cookies or tracking agents at all.
- The site uses local fonts to help prevent tracking.
- No JavaScript either. Pure heaven - exactly how the Internet should be!!
