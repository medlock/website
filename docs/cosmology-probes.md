---
title: Unveiling the Universe with Emerging Cosmological Probes
type: post
ref: Published 2022-02-01
tags: physics, cosmology, Hubble
---

I've been wading through [arXiv:2201.07241v1](https://arxiv.org/abs/2201.07241v1) paper on "Unveiling the Universe with Emerging Cosmological Probes".

Here is the abstract -

<p class="Abst">
The detection of the accelerated expansion of the Universe has been one of the major breakthroughs in modern cosmology. Several cosmological probes (CMB, SNe Ia, BAO) have been studied in depth to better understand the nature of the mechanism driving this acceleration, and they are being currently pushed to their limits, obtaining remarkable constraints that allowed us to shape the standard cosmological model. In parallel to that, however, the percent precision achieved has recently revealed apparent tensions between measurements obtained from different methods. These are either indicating some unaccounted systematic effects, or are pointing toward new physics. Following the development of CMB, SNe, and BAO cosmology, it is critical to extend our selection of cosmological probes. Novel probes can be exploited to validate results, control or mitigate systematic effects, and, most importantly, to increase the accuracy and robustness of our results.*
<p>
<p class="Abst">
This review is meant to provide a state-of-art benchmark of the latest advances in emerging beyond-standard cosmological probes. We present how several different methods can become a key resource for observational cosmology. In particular, we review cosmic chronometers, quasars, gamma-ray bursts, standard sirens, lensing time-delay with galaxies and clusters, cosmic voids, neutral hydrogen intensity mapping, surface brightness fluctuations, secular redshift drift, and clustering of standard candles. The review describes the method, systematics, and results of each probe in a homogeneous way, giving the reader a clear picture of the available innovative methods that have been introduced in recent years and how to apply them. The review also discusses the potential synergies and complementarities between the various probes, exploring how they will contribute to the future of modern cosmology.* 
</p>

---

<br>
This is a lengthy paper (192 pages, including 66 pages of references!) but well worth the read as it goes through the methods of determining cosmological parameters. Despite its length it's in a very easy to read and logical format.

It documents each method, an overview of the basic idea of maths and physics underpinning it, and the pros and cons of each.


| Method | Page |
|:---|:---:|
| Cosmic Chronometers		|  8|
| Quasars					| 22|
| Gamma-Ray Bursts			| 30|
| Standard Sirens 			| 42|
| Time Delay Cosmography	| 49|
| Cosmography with Cluster Strong Lensing	| 58|
| Cosmic Voids 				| 67|
| Neutral Hydrogen Intensity Mapping 	| 79|
| Surface Brightness Fluctuations 		| 91|
| Stellar Ages				| 101|
| Secular Redshift Drift 	| 108|
| Clustering of Standard Candles 		|114|


Below is the table shown on page 122 of the article summarising the probes, what they measure, along with the strengths and weaknesses.

<br>

<img src="/resources/probes.png"/>
