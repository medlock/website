---
title: Standard Sirens Update
type: post
ref: Published 2022-01-11
tags: physics, cosmology, Hubble, sirens
---

[This article](https://astrobites.org/2022/01/04/gw-cosmo/) in [Astrobites](https://astrobites.org) gives an update on the use of gravitational waves to determine the Hubble parameter [H~0~](https://en.wikipedia.org/wiki/Hubble%27s_law) .

Here's the abstract

<p class="Abst">
We use 47 gravitational-wave sources from the Third LIGO-Virgo-KAGRA Gravitational-Wave Transient Catalog (GWTC-3) to estimate the Hubble parameter H(z), including its current value, the Hubble constant H0. Each gravitational-wave (GW) signal provides the luminosity distance to the source and we estimate the corresponding redshift using two methods: the redshifted masses and a galaxy catalog. Using the binary black hole (BBH) redshifted masses, we simultaneously infer the source mass distribution and H(z). The source mass distribution displays a peak around 34M⊙, followed by a drop-off. Assuming this mass scale does not evolve with redshift results in a H(z) measurement, yielding H0=68+12−7kms−1Mpc−1 (68% credible interval) when combined with the H0 measurement from GW170817 and its electromagnetic counterpart. This represents an improvement of 17% with respect to the H0 estimate from GWTC-1. The second method associates each GW event with its probable host galaxy in the catalog GLADE+, statistically marginalizing over the redshifts of each event's potential hosts. Assuming a fixed BBH population, we estimate a value of H0=68+8−6kms−1Mpc−1 with the galaxy catalog method, an improvement of 42% with respect to our GWTC-1 result and 20% with respect to recent H0 studies using GWTC-2 events. However, we show that this result is strongly impacted by assumptions about the BBH source mass distribution; the only event which is not strongly impacted by such assumptions (and is thus informative about H0) is the well-localized event GW190814. 
</p>


Original article [ArXiv:2111.03604](https://arxiv.org/abs/2111.03604)

This method uses gravitational waves to determine the distance, however, spectra from light of the event is analysed to determine the redshift (for which there is only currently one event - GW190814). So, in addition, they have used an existing catalogue to try and match spectra to gravitational events to determine redshift. This has affected the confidence in the value and hence the error bars.

In contrast SHOES uses purely light to determine distance and redshift.

It's worth noting that determining distance has traditionally been troublesome in astronomy. For large distances it is normally calculated by the apparent brightness of an object. This has inherent difficulties, not least, dust can affect the reading (making an object appear darker than it should - and hence larger distances inferred.). 

In the SHOES work (link below) they have gone to great lengths to try and mitigate this.

<img src="https://astrobites.org/wp-content/uploads/2022/01/Screenshot-2022-01-03-at-23-01-57-2111-03604-pdf.png" id="fig_pic_left"/>

<br>

Left figure shows the value that they arrived at (in black), along with other methods (ie Planck - pink, SHOES - green).


Related post - 

[SHOES - Local Hubble Parameter with 1 kms/s/Mpc Uncertainty (2021-12-16)](/docs/cosmology-Local-H0.html)

<br>
<br>
<br>
<br>
<br>
