---
title: Backgammon
type: post
ref: Published 2022-01-12
tags: backgammon, Ur, xg, wine
---

[Backgammon](https://en.wikipedia.org/wiki/Backgammon) is a great game! 

I prefer it to chess simply because of the random element of the game. One has to adapt. For me, chess is just too ridged! 

Often referred to as the "cruellest game", directly because one is not guaranteed victory until the very end. To me, this is what makes it exciting, and sometimes deeply frustrating. Victory of a game (usually not a match) can be snatched away by a lucky player. 

I tend to play 15 or 21 point matches. This is to iron-out the potential luck element in lower point matches, whereby the luckiest player can win. The longer point matches tend to remove the luck element and the greater skilled player tends to win. But this is not guaranteed! 

Remember, we are dealing with probabilities. So, if it can happen, it will happen - eventually!

For me, backgammon has the right mix of luck and skill. [The Royal Game of Ur](https://en.wikipedia.org/wiki/Royal_Game_of_Ur) is another game I like which is very similar to backgammon.

They are essentially race games coupled with skill & strategy. 

<br>
In an effort to improve my game I'm going to start publishing some analysed games.

I will show here in due course.

<br>
If you fancy a game, get in touch.

I prefer face to face games as it is more interesting and less prone to cheating. It's just more sociable that way, particular in the pub over a beer. 

<br>
Good online sites:-

- [Daily Gammon](http://dailygammon.com)

- [Next Gammon](https://nextgammon.com/)

- [Backgammon Studio Heroes](https://heroes.backgammonstudio.com/)


Good Backgammon Software:-

- [Extreme Gammon](http://www.extremegammon.com/) - paid (Windoze only!)^1^

- [GNU Backgammon](http://www.gnubg.org/)  - free  

- [BGBlitz](https://www.bgblitz.com)  - free


**Note**


^1^ Extreme Gammon is Windoze based but it can be run in [Wine](https://www.winehq.org/). 

  This is what I do and it works perfectly well.

